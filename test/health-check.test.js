const tap = require('tap')
const request = require('supertest');

const SvcAuth = require('./mock/svc-auth');

tap.before(async () => {
  const svcAuth = await SvcAuth();
  Object.assign(this, { svcAuth })
});

tap.teardown(() => this.svcAuth.stop());

tap.test('GET /api/v1/health-check', async (t) => {
  await request(this.svcAuth.fastify.server)
    .get('/api/v1/health-check')
    .expect('Content-Type', /json/)
    .expect(200)
})
