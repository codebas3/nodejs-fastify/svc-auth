const dump = {}

const { env } = process;

function mockAppPort(contain, value) {
  const key = contain.toUpperCase();
  const kHttpPort = `${key}_HTTP_PORT`;
  const kGrpcPort = `${key}_GRPC_PORT`;
  const kSvcUrl = `${key}_SVC_URL`;
  const kRpcUrl = `${key}_RPC_URL`;

  if (!dump[key]) {
    dump[key] = {
      [kHttpPort]: env[kHttpPort],
      [kGrpcPort]: env[kGrpcPort],
      [kSvcUrl]: env[kSvcUrl],
      [kRpcUrl]: env[kRpcUrl]
    }
  }

  const grpc = `${value}0`
  process.env[kHttpPort] = value
  process.env[kGrpcPort] = grpc
  process.env[kSvcUrl] = `http://localhost:${value}`
  process.env[kRpcUrl] = `localhost:${grpc}`
}

function resetAppPort(contain) {
  const key = contain.toUpperCase();
  if (dump[key]) {
    const kHttpPort = `${key}_HTTP_PORT`;
    const kGrpcPort = `${key}_GRPC_PORT`;
    const kSvcUrl = `${key}_SVC_URL`;
    const kRpcUrl = `${key}_RPC_URL`;

    process.env[kHttpPort] = dump[key][kHttpPort]
    process.env[kGrpcPort] = dump[key][kGrpcPort]
    process.env[kSvcUrl] = dump[key][kSvcUrl]
    process.env[kRpcUrl] = dump[key][kRpcUrl]
  }
}

module.exports = {
  mockAppPort,
  resetAppPort
}
