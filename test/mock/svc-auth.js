require('./variable').mockAppPort('auth', '2000')

const Manifest = require('../../src/manifest');

const manifest = Manifest.getInstance()

module.exports = async () => {
  await manifest.setup();
  await manifest.start();

  manifest.fastify = manifest.server.app;

  return manifest;
};
