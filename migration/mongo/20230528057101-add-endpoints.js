const Promise = require('bluebird');
const { app, hosts } = require('../../src/config')
const { unixNow } = require('../../src/util/timezone');
const ResServerMigration = require('./20230526055101-add-resource-servers')
const RoleMigration = require('./20230528055101-add-roles')

const getEndpoints = async (db) => {
  const [resServer] = await ResServerMigration.getExisting(db)
  const roles = await RoleMigration.getExisting(db)

  const endpoints = [
    {
      code: 'scope:read',
      name: 'Read all of scopes',
      isInternalDefault: true,
      isExternalDefault: false,
      resourceServerId: resServer._id,
      roleIds: roles.filter(role => role.isExternalDefault === false).map(role => role._id)
    }
  ]

  return endpoints;
}

const getExisting = async (db) => {
  const endpoints = await getEndpoints(db)
  const selection = await db.collection('endpoint').find({
    code: {
      $in: endpoints.map(el => el.code)
    }
  }).limit(Infinity).toArray();

  return selection
}

module.exports = {
  getEndpoints,
  getExisting,
  async up(db, client) {
    const existData = await getExisting(db)

    let endpoints = []
    if (existData.length) {
      endpoints = existData;
    } else {
      endpoints = await getEndpoints(db)
    }

    return existData.length ? true : Promise.all(endpoints.map((payload) => db.collection('endpoint').updateOne(
      { code: payload.code },
      {
        $setOnInsert: {
          ...payload,
          createdAt: unixNow(),
          createdBy: 'SYSTEM'
        }
      },
      { upsert: true }
    )))
  },

  async down(db, client) {
    const endpoints = await getEndpoints(db)

    return db.collection('endpoint').deleteMany({
      _id: {
        $in: endpoints.map((el) => el.client_id)
      }
    })
  }
};
