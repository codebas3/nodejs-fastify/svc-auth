const Promise = require('bluebird');
const { app, hosts } = require('../../src/config')
const { enumeration: { scopes } } = require('../../src/model/mongo/resource_server')
const { unixNow } = require('../../src/util/timezone');

const getResServers = async () => {
  const resServers = [
    {
      code: app.packageName,
      name: app.description,
      uri: hosts.svc.auth,
      isPublic: false,
      scopes,
    }
  ]

  return resServers;
}

const getExisting = async (db) => {
  const resServers = await getResServers()
  const selection = await db.collection('resource_server').find({
    code: {
      $in: resServers.map(el => el.code)
    }
  }).limit(Infinity).toArray();

  return selection
}

module.exports = {
  getResServers,
  getExisting,
  async up(db, client) {
    const existData = await getExisting(db)

    let resServers = []
    if (existData.length) {
      resServers = existData;
    } else {
      resServers = await getResServers()
    }

    return existData.length ? true : Promise.all(resServers.map((payload) => db.collection('resource_server').updateOne(
      { code: payload.code },
      {
        $setOnInsert: {
          ...payload,
          createdAt: unixNow(),
          createdBy: 'SYSTEM'
        }
      },
      { upsert: true }
    )))
  },

  async down(db, client) {
    const resServers = await getResServers()

    return db.collection('resource_server').deleteMany({
      _id: {
        $in: resServers.map((el) => el.client_id)
      }
    })
  }
};
