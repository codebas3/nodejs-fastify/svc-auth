const Promise = require('bluebird');
const { unixNow } = require('../../src/util/timezone');

const getRoles = async (db) => {
  const endpoints = [
    {
      code: 'sys-owner',
      name: 'System Owner',
      isInternalDefault: false,
      isExternalDefault: false
    },
    {
      code: 'sys-admin',
      name: 'System Admin',
      isInternalDefault: false,
      isExternalDefault: false
    },
    {
      code: 'sys-office',
      name: 'System Office',
      isInternalDefault: true,
      isExternalDefault: false
    },
    {
      code: 'user',
      name: 'User',
      isInternalDefault: false,
      isExternalDefault: true,
    }
  ]

  return endpoints;
}

const getExisting = async (db) => {
  const endpoints = await getRoles(db)
  const selection = await db.collection('role').find({
    code: {
      $in: endpoints.map(el => el.code)
    }
  }).limit(Infinity).toArray();

  return selection
}

module.exports = {
  getRoles,
  getExisting,
  async up(db, client) {
    const existData = await getExisting(db)

    let endpoints = []
    if (existData.length) {
      endpoints = existData;
    } else {
      endpoints = await getRoles(db)
    }

    return existData.length ? true : Promise.all(endpoints.map((payload) => db.collection('role').updateOne(
      { code: payload.code },
      {
        $setOnInsert: {
          ...payload,
          createdAt: unixNow(),
          createdBy: 'SYSTEM'
        }
      },
      { upsert: true }
    )))
  },

  async down(db, client) {
    const endpoints = await getRoles(db)

    return db.collection('role').deleteMany({
      _id: {
        $in: endpoints.map((el) => el.client_id)
      }
    })
  }
};
