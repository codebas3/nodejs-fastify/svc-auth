const { env } = process;
const fs = require('fs')
const path = require('path');
const Promise = require('bluebird');
const { authClient, mailStandard } = require('../../src/util')
const { app, hosts } = require('../../src/config')
const { enumeration: { scopes } } = require('../../src/model/mongo/client')
const ResServerMigration = require('./20230526055101-add-resource-servers')
const { unixNow } = require('../../src/util/timezone');

const dirName = path.resolve(__dirname, '..', '..', '.data');
const fileName = path.resolve(__dirname, '..', '..', '.data', 'clients.json');
const internals = [
  `${env.WEB_APP_ADMIN_URL}/callback`
]

if (!fs.existsSync(dirName)) {
  fs.mkdirSync(dirName);
}

const getOAuthClients = async (db) => {
  const resServers = await ResServerMigration.getExisting(db)
  const resourceScopes = [];

  resServers.forEach(rs => {
    rs.scopes.forEach(scope => {
      resourceScopes.push(`${rs.code}:${scope}`)
    })
  })

  const clients = [
    {
      client_name: 'Auth WebApp',
      client_id: 'Du8c84mZqXwMLeVLHx8UWJMt',
      client_secret: authClient.newSecret(),
      redirect_uris: [`${hosts.svc.auth}/callback`],
      post_logout_redirect_uris: [`${hosts.svc.auth}/dashboard`],
      grant_types: ['authorization_code', 'password', 'refresh_token'],
      scope: scopes.join(' '),
      resource_scope: resourceScopes.join(' '),
      is_internal: true
    },
    {
      client_name: 'Auth WebApp Admin',
      client_id: 'mQ9tFSz6QQVE7QhkBpDts6Tj',
      client_secret: authClient.newSecret(),
      redirect_uris: [`${env.WEB_APP_ADMIN_URL}/callback`],
      post_logout_redirect_uris: [`${env.WEB_APP_ADMIN_URL}/dashboard`],
      grant_types: ['authorization_code', 'password', 'refresh_token'],
      scope: scopes.join(' '),
      resource_scope: resourceScopes.join(' '),
      is_internal: true
    }
  ]

  if (env.NODE_ENV !== 'production') {
    clients.forEach((client) => {
      const uri = client.redirect_uris[0];
      if (internals.indexOf(uri) > -1) {
        // const protocol = (uri.match(/http(s)?:\/\//) || [])[0]
        const port = ((uri.match(/:[0-9]+/) || [])[0] || '').substring(1);
        const localURI = `http://localhost${port ? `:${port}` : ''}`
        if (uri.indexOf(localURI) === -1) client.redirect_uris.push(`${localURI}/callback`)
      }
    });
    clients.unshift({
      client_id: 'aio-client',
      client_name: 'Example',
      client_secret: authClient.newSecret(),
      redirect_uris: ['http://localhost:3030/callback', 'https://oidcdebugger.com/debug'],
      post_logout_redirect_uris: ['http://localhost:3030/dashboard'],
      grant_types: ['authorization_code', 'password', 'refresh_token', 'client_credentials'],
      scope: scopes.join(' '),
      resource_scope: resourceScopes.join(' '),
      is_internal: true
    })
  }

  return clients;
}

const getExisting = async (db) => {
  const oauthClients = await getOAuthClients(db)
  const selection = await db.collection('client').find({
    client_id: {
      $in: oauthClients.map(el => el.client_id)
    }
  }).limit(Infinity).toArray();

  return selection
}

module.exports = {
  getOAuthClients,
  getExisting,
  async up(db, client) {
    const existData = await getExisting(db)

    let oauthClients = []
    if (existData.length) {
      oauthClients = existData;
    } else {
      oauthClients = await getOAuthClients(db)
    }
    const content = JSON.stringify(oauthClients, 0, 2)

    fs.writeFileSync(fileName, content)
    await mailStandard.send({
      subject: `${app.project}: Client credential (${fileName})`,
      text: `Here is oauth client credentials at ${hosts.svc.auth}:\n${content}`,
      html: `Here is oauth client credentials at <a href="${hosts.svc.auth}">${hosts.svc.auth}</a>:<br/><pre>${content}</pre>`
    })

    return existData.length ? true : Promise.all(oauthClients.map((payload) => db.collection('client').updateOne(
      { _id: payload.client_id },
      {
        $setOnInsert: {
          payload,
          createdAt: unixNow(),
          createdBy: 'SYSTEM'
        }
      },
      { upsert: true }
    )))
  },

  async down(db, client) {
    const oauthClients = await getOAuthClients(db)

    fs.unlinkSync(fileName, '')

    return db.collection('client').deleteMany({
      _id: {
        $in: oauthClients.map((el) => el.client_id)
      }
    })
  }
};
