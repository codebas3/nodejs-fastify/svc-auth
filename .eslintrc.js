module.exports = {
  env: {
    browser: true,
    commonjs: true,
    es2021: true,
  },
  extends: ['airbnb-base', 'plugin:tap/recommended'],
  parserOptions: {
    ecmaVersion: 13,
    sourceType: 'module',
  },
  plugins: ['tap', 'eslint-plugin-html', 'eslint-plugin-json'],
  rules: {
    'max-len': ['warn', 160],
    indent: ['error', 2, { ignoreComments: true }],
    'comma-dangle': ['off', 'never'],
    semi: ['off', 'never'],
    'linebreak-style': 0,
    radix: 'off',
    'func-names': 'off',
    'arrow-parens': 'off',
    'global-require': 'off',
    'no-unused-vars': 'off',
    'no-underscore-dangle': 'off',
    'no-await-in-loop': 'off',
    'no-plusplus': ['error', { allowForLoopAfterthoughts: true }],
    'import/extensions': 'off',
    'import/no-dynamic-require': 'off',
    'import/no-extraneous-dependencies': 'off',
    'import/no-unresolved': 'warn',
    'guard-for-in': 'warn',
    'no-restricted-syntax': 'warn',
    'no-continue': 'warn',
    'no-param-reassign': 'warn',
    'newline-per-chained-call': 'warn',
    'no-nested-ternary': 'warn',
    'no-multi-assign': 'warn',
    'no-console': 'off',

    // tap
    'tap/assertion-message': ['error', 'always'],
    'tap/max-asserts': ['error', 8],
    'tap/no-identical-title': 'error',
    'tap/no-ignored-test-files': 'error',
    'tap/no-only-test': 'error',
    'tap/no-skip-test': 'error',
    'tap/no-statement-after-end': 'error',
    'tap/use-plan-well': 'error',
    'tap/use-t-well': 'error',
    'tap/use-t': 'error',
    'tap/use-tap': 'error',

    // tap: annoying eslint config! let's make this silent
    // 'tap/test-title': ['error', 'if-multiple'] // this is unknown config, idk?!
    'tap/no-unknown-modifiers': 'warn', // 'error'
    'tap/test-ended': 'warn', // 'error'
    'tap/use-plan': 'warn', // ['error', 'always'],
  },
  ignorePatterns: [
    'Dockerfile',
    'node_modules',
    'dist',
    'src/page/asset',
    'src/page/css',
    'src/page/lib',
    '*.proto',
    '*.ejs',
    '*.md',
    '*.sh'
  ],
  settings: {
    'import/resolver': {
      node: {
        moduleDirectory: ['node_modules'],
        paths: ['.'],
      },
    },
  },
}
