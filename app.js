const logger = require('./src/logger/console')(__filename);
const { stringify, objError } = require('./src/logger/lib/format');
const { constants: { TYPE_ERROR } } = require('./src/config');
const Manifest = require('./src/manifest');

const start = async () => {
  const manifest = Manifest.getInstance();

  await manifest.setup();
  manifest.start();
}

async function exitHandler(options, reason, code) {
  if (options.cleanup) {
    // cleanup logic here..
  }
  if (reason.message) logger.error(stringify(TYPE_ERROR.UNCAUGHT, objError(reason)));
  if (code || code === 0) console.info(`Process exited with code: ${code}`);
  if (options.exit) process.exit();
}

start();

process
  /** Do something when app is closing */
  .on('exit', exitHandler.bind(null, { cleanup: true }))

  /** Catches ctrl+c event */
  .on('SIGINT', exitHandler.bind(null, { cleanup: true, exit: true }))

  /** Catches "kill pid" (for example: nodemon restart) */
  .on('SIGUSR1', exitHandler.bind(null, { cleanup: true, exit: true }))
  .on('SIGUSR2', exitHandler.bind(null, { cleanup: true, exit: true }))

  /** Caught all unhandled rejected promise exceptions */
  .on('unhandledRejection', (reason, p) => {
    throw reason;
  })
  /** Caught all unhandled non-promise exceptions */
  .on('uncaughtException', exitHandler.bind(null, { cleanup: true, exit: true }))
