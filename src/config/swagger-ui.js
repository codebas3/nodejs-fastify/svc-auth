const onComplete = function () {
  // This block code will run under browser
  console.info('Welcome to swagger ui')
  setInterval(() => {
    document.querySelectorAll('.response .response-col_status').forEach((el, i) => {
      el.style.paddingTop = '17px'
      el.style.fontSize = '14px'
    })
    document.querySelectorAll('.response-col_description__inner .renderedMarkdown > p').forEach((el, i) => {
      el.style.marginTop = '5px'
      el.style.marginBottom = '0px'
    })
    document.querySelectorAll('.swagger-ui .model-title').forEach((el, i) => {
      el.style.fontSize = '14px'
    })
  }, 1000)
}

module.exports = {
  routePrefix: '/api/docs',
  uiConfig: {
    docExpansion: 'full',
    deepLinking: true,
    onComplete,
  },
  uiHooks: {
    onRequest(request, reply, next) { next() },
    preHandler(request, reply, next) { next() }
  },
  staticCSP: true,
  transformStaticCSP: (header) => header,
  transformSpecification: (swaggerObject, req, res) => {
    swaggerObject.host = req.hostname
    return swaggerObject
  },
  transformSpecificationClone: true
}
