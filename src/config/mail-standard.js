require('dotenv').config();

const { env } = process
const port = parseInt(env.AUTH_MAIL_PORT || env.MAIL_PORT)
const receiver = env.AUTH_MAIL_RECEIVER || env.MAIL_RECEIVER
const sender = env.AUTH_MAIL_SENDER || env.MAIL_SENDER

module.exports = {
  sender: {
    name: env.AUTH_MAIL_SENDER_NAME || env.MAIL_SENDER_NAME,
    email: sender
  },
  receiver: {
    email: receiver || sender
  },
  transport: {
    host: env.AUTH_MAIL_HOST || env.MAIL_HOST,
    port,
    secure: port === 465,
    auth: {
      user: env.AUTH_MAIL_USERNAME || env.MAIL_USERNAME,
      pass: env.AUTH_MAIL_PASSWORD || env.MAIL_PASSWORD
    },
    tls: { rejectUnauthorized: false },
    debug: true
  }
}
