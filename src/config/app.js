require('dotenv').config()

const { env } = process
const {
  name, description, codename, version
} = require('../../package.json')

module.exports = {
  project: env.PROJECT_NAME || env.PROJECT_NAME,
  name: env.AUTH_APP_NAME || env.APP_NAME,
  port: env.AUTH_HTTP_PORT || env.HTTP_PORT,
  codename,
  packageName: name,
  version,
  description,
  logLevel: env.AUTH_LOG_LEVEL || env.LOG_LEVEL || 'debug',
  cookies: (env.AUTH_SECRET_COOKIES || env.SECRET_COOKIES).split(',')
}
