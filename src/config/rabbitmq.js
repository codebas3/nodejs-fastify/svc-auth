require('dotenv').config()

const { env } = process

module.exports = {
  port: env.AUTH_RABBIT_MQ_PORT || env.RABBIT_MQ_PORT,
  host: env.AUTH_RABBIT_MQ_HOST || env.RABBIT_MQ_HOST,
  user: env.AUTH_RABBIT_MQ_USERNAME || env.RABBIT_MQ_USERNAME,
  password: env.AUTH_RABBIT_MQ_PASSWORD || env.RABBIT_MQ_PASSWORD,
  queue: {
    ResourceServer: {
      exchange: {
        name: 'resource-server',
        type: 'topic',
        options: {
          durable: true
        }
      },
      route: 'resource-server',
      // For service cluster which mean "svc-yourbackend" has multiple instances
      // Scenario 1: You should add more prefix if you need every instance of subscription/consumer receive the message
      // Scenario 2: Don't add more prefix if your service cluster just want to receive one only from many (of same instance)
      queue: 'svc-auth:resource-server'
    },
    Client: {
      exchange: {
        name: 'client',
        type: 'topic',
        options: {
          durable: true
        }
      },
      route: 'client',
      queue: 'svc-auth:client'
    },
    Role: {
      exchange: {
        name: 'role',
        type: 'topic',
        options: {
          durable: true
        }
      },
      route: 'role',
      queue: 'svc-auth:role'
    },
    Endpoint: {
      exchange: {
        name: 'endpoint',
        type: 'topic',
        options: {
          durable: true
        }
      },
      route: 'endpoint',
      queue: 'svc-auth:endpoint'
    }
  }
};
