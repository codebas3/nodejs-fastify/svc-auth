require('dotenv').config()

const { env } = process

const config = {
  username: env.AUTH_MONGO_USERNAME || env.MONGO_USERNAME,
  password: env.AUTH_MONGO_PASSWORD || env.MONGO_PASSWORD,
  database: env.AUTH_MONGO_DBNAME || env.MONGO_DBNAME,
  host: env.AUTH_MONGO_HOST || env.MONGO_HOST,
  port: env.AUTH_MONGO_PORT || env.MONGO_PORT,
  pool: {
    bufferCommands: true,
    minPoolSize: 0,
    maxPoolSize: 5,
    connectTimeoutMS: 30000, // default: 30000
    socketTimeoutMS: 30000, // default: 30000
    serverSelectionTimeoutMS: 30,
    heartbeatFrequencyMS: 1000
  },
  family: 4,
  useCreateIndex: true,
  autoIndex: true,
  logConnection: true
}

if (env.NODE_ENV === 'production') {
  config.logging = false
}

module.exports = config
