require('dotenv').config()

const { env } = process

module.exports = {
  env: env.NODE_ENV,
  basePath: require('path').resolve(__dirname, '..', '..'),
  app: require('./app'),
  grpc: require('./grpc'),
  hosts: require('./hosts'),
  auth: require('./auth'),
  sql: require('./sql'),
  sqlTables: require('./sqlTables'),
  mongo: require('./mongo'),
  mongoCollections: require('./mongoCollections'),
  message: require('./message'),
  mailStandard: require('./mail-standard'),
  redis: require('./redis'),
  rabbitmq: require('./rabbitmq'),
  cron: require('./cron'),
  io: require('./io'),
  openAPI: require('./open-api'),
  swaggerUI: require('./swagger-ui'),
  constants: require('./constants'),
}
