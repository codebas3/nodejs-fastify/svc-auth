const { codename } = require('../app')

exports.test = `${codename}Msg000`;
exports.invalidSessionFormat = `${codename}Msg001`;
exports.invalidSessionExp = `${codename}Msg002`;
exports.invalidSession = `${codename}Msg003`;
exports.invalidIssuer = `${codename}Msg004`;
exports.invalidIdentity = `${codename}Msg004`;
exports.accessProhibited = `${codename}Msg005`;
