exports.test = (name) => `Hello "${name}"! This is message example`;
exports.invalidSessionFormat = 'Invalid session (value)'
exports.invalidSessionExp = 'Invalid session (expired)';
exports.invalidSession = 'Invalid session'
exports.invalidIssuer = 'Invalid issuer'
exports.invalidIdentity = 'Invalid identity'
exports.accessProhibited = 'Access prohibited'
