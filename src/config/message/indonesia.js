exports.test = (name) => `Hai "${name}"! Ini adalah contoh pesan`;
exports.invalidSessionFormat = 'Sesi tidak valid (value)';
exports.invalidSessionExp = 'Sesi tidak valid (expired)';
exports.invalidSession = 'Sesi tidak valid'
exports.invalidIssuer = 'Issuer tidak valid'
exports.invalidIdentity = 'Identitas tidak valid'
exports.accessProhibited = 'Akses tidak diperbolehkan'
