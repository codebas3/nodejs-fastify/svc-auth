require('dotenv').config()

const { env } = process

module.exports = {
  cronTime: env.AUTH_CRON_TIME || env.CRON_TIME || '0 0 * * * *'
}
