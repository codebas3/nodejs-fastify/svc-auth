require('dotenv').config();

const { env } = process

module.exports = {
  host: env.AUTH_REDIS_HOST || env.REDIS_HOST,
  port: env.AUTH_REDIS_PORT || env.REDIS_PORT,
  family: 4, // 4 (IPv4) or 6 (IPv6)
  db: env.AUTH_REDIS_DBNAME || env.REDIS_DBNAME || 0,
  username: env.AUTH_REDIS_USERNAME || env.REDIS_USERNAME,
  password: env.AUTH_REDIS_PASSWORD || env.REDIS_PASSWORD,
}
