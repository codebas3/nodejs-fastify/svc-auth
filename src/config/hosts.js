require('dotenv').config();

const { env } = process;

module.exports = {
  svc: {
    auth: env.AUTH_SVC_URL || env.SVC_URL,
    user: env.USER_SVC_URL || env.USER_SVC_URL,
    notify: env.NOTIFY_SVC_URL || env.NOTIFY_SVC_URL
  },
  rpc: {
    auth: env.AUTH_RPC_URL || env.RPC_URL,
    user: env.USER_RPC_URL || env.USER_RPC_URL,
    notify: env.NOTIFY_RPC_URL || env.NOTIFY_RPC_URL

  }
}
