const joi2json = require('joi-to-json')
// const { getReasonPhrase } = require('http-status-codes')

const config = require('../config')
const { Common: SchemaV1 } = require('./v1/schema')

Object.entries(SchemaV1).forEach(([key, value]) => {
  config.openAPI.components.schemas[value.name || key] = joi2json(value.schema, 'open-api')
})

const ROUTE_DEF = {
  tags: [],
  summary: '',
  description: '',
  parameters: [],
  responses: {}
}

Object.entries(SchemaV1).forEach(([httpCode, schema]) => {
  if (!parseInt(httpCode) || parseInt(httpCode) === '200') return;

  ROUTE_DEF.responses[httpCode] = {
    description: `<b><mark>${schema.name}</mark></b> schema`,
    /**
     * * Uncomment if you want to show schema;
     * * and, comment it if other else
     */
    // content: {
    //   'application/json': {
    //     schema: {
    //       $ref: `#/components/schemas/${getReasonPhrase(httpCode).replace(/\s/g, '')}`
    //     }
    //   }
    // }
  }
})

module.exports = {
  DOCS_DEF: config.openAPI,
  ROUTE_DEF
}
