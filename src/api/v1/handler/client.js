const qsToSQL = require('../../../util/qs-to-sql');
const { v1: { Client: Service } } = require('../../../service');

const create = async (req, res) => {
  const service = new Service()
  const result = await service.create(req.body, null, req.usr.id);

  return res.send({
    statusCode: 200,
    data: result
  });
};

const getOneBy = async (req, res) => {
  const service = new Service()
  const options = {
    where: { _id: req.params._id },
    count: true
  }
  const result = await service.getOneBy(options);

  return res.send({
    statusCode: 200,
    data: result
  });
};

const getBy = async (req, res) => {
  const service = new Service()
  const options = {
    ...qsToSQL.build(req.query),
    count: true
  }
  const result = await service.getBy(options);
  return res.send({
    statusCode: 200,
    data: result
  });
};

const deleteOneBy = async (req, res) => {
  const service = new Service()
  const options = {
    where: { _id: req.params._id }
  }
  const result = await service.deleteBy(options, req.usr.id);

  return res.send({
    statusCode: 200,
    data: result
  });
};

const updateOneBy = async (req, res) => {
  const service = new Service()
  const options = {
    where: { _id: req.params._id }
  }
  const result = await service.updateBy(req.body, options, req.usr.id);

  return res.send({
    statusCode: 200,
    data: result
  });
};

module.exports = {
  create,
  getOneBy,
  getBy,
  deleteOneBy,
  updateOneBy
};
