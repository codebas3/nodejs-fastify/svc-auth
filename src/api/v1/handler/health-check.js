const { v1: { HealthCheck: HealthCheckService } } = require('../../../service');

const checkAll = async (req, res, next) => {
  try {
    const healthCheckService = new HealthCheckService()
    const result = await healthCheckService.checkAll();

    return res.send({
      statusCode: 200,
      data: result
    });
  } catch (e) {
    return next()
  }
};

module.exports = { checkAll };
