const { v1: { Scope: Service } } = require('../../../service');

const getBy = async (isPublic) => {
  const service = new Service()
  const options = {
    count: false,
    limit: Infinity
  }
  if (isPublic) options.where = { isPublic };

  return service.getBy(options);
};

const getInternal = async (_req, res) => res.send({
  statusCode: 200,
  data: await getBy(true)
});

const getAll = async (_req, res) => res.send({
  statusCode: 200,
  data: await getBy()
});

module.exports = {
  getAll,
  getInternal
};
