module.exports = {
  HealthCheck: require('./health-check'),
  Client: require('./client'),
  Scope: require('./scope'),
  ResourceServer: require('./resource-server'),
  Endpoint: require('./endpoint'),
  Role: require('./role'),
};
