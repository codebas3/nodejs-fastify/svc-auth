module.exports = {
  HealthCheck: require('./health-check'),
  Client: require('./client'),
  ResourceServer: require('./resource-server'),
  Scope: require('./scope'),
  Endpoint: require('./endpoint'),
  Role: require('./role'),
};
