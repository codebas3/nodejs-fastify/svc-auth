const { Scope: Handler } = require('../handler');
const { authentication, authorization } = require('../middleware');
const { Scope: Schema, Common: CommonSchema } = require('../schema')

const definition = {
  name: 'Scope',
  basePath: '/api/v1/scope',
  description: 'Scope APIs v1',
  routes: [
    {
      enabled: true,
      method: 'get',
      path: '/all',
      summary: 'List of all scopes',
      description: 'Get of all scopes',
      action: [
        authentication,
        authorization('api:read', ['scope:read']),
        Handler.getAll
      ],
      validators: {
        headers: CommonSchema.Header.schema
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: Scope',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
    {
      enabled: true,
      method: 'get',
      path: '/',
      summary: 'List of public scopes',
      description: 'List of public scopes',
      action: [
        authentication,
        Handler.getInternal
      ],
      validators: {
        headers: CommonSchema.Header.schema
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: Scope',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
  ]
}

module.exports = definition;
