const { ResourceServer: Handler } = require('../handler');
const { authentication, authorization } = require('../middleware');
const { ResourceServer: Schema, Common: CommonSchema } = require('../schema')

const definition = {
  name: 'ResourceServer',
  basePath: '/api/v1/resource-server',
  description: 'ResourceServer APIs v1',
  routes: [
    {
      enabled: true,
      method: 'post',
      path: '/',
      summary: 'Add new resource server',
      description: 'Create new resource server data',
      action: [
        authentication,
        authorization('api:add', ['resource-server:add']),
        Handler.create
      ],
      validators: {
        headers: CommonSchema.Header.schema,
        body: Schema.body
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: ResourceServer',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
    {
      enabled: true,
      method: 'get',
      path: '/:_id',
      summary: 'Get a resource server',
      description: 'Get resource server by id',
      action: [
        authentication,
        authorization('api:read', ['resource-server:read']),
        Handler.getOneBy
      ],
      validators: {
        headers: CommonSchema.Header.schema,
        // ? Why #ResourceServerParams Schema didn't appear in Swagger UI
        params: Schema.params
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: ResourceServer',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
    {
      enabled: true,
      method: 'get',
      path: '/',
      summary: 'List of resource servers',
      description: 'Get multiple resource servers',
      action: [
        authentication,
        authorization('api:read', ['resource-server:read']),
        Handler.getBy
      ],
      validators: {
        headers: CommonSchema.Header.schema,
        query: CommonSchema.Query.schema
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: ResourceServer',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
    {
      enabled: true,
      method: 'delete',
      path: '/:_id',
      summary: 'Delete a resource server',
      description: 'Delete a resource server by id',
      action: [
        authentication,
        authorization('api:remove', ['resource-server:remove']),
        Handler.deleteOneBy
      ],
      validators: {
        headers: CommonSchema.Header.schema,
        // ? Why #ResourceServerParams Schema didn't appear in Swagger UI
        params: Schema.params
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: ResourceServer',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
    {
      enabled: true,
      method: 'put',
      path: '/:_id',
      summary: 'Update a resource server',
      description: 'Update a resource server by id',
      action: [
        authentication,
        authorization('api:update', ['resource-server:update']),
        Handler.updateOneBy
      ],
      validators: {
        headers: CommonSchema.Header.schema,
        // ? Why #ResourceServerParams Schema didn't appear in Swagger UI
        params: Schema.params,
        body: Schema.body
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: ResourceServer',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
  ]
}

module.exports = definition;
