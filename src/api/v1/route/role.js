const { Role: Handler } = require('../handler');
const { authentication, authorization } = require('../middleware');
const { Role: Schema, Common: CommonSchema } = require('../schema')

const definition = {
  name: 'Role',
  basePath: '/api/v1/role',
  description: 'Role APIs v1',
  routes: [
    {
      enabled: true,
      method: 'post',
      path: '/',
      summary: 'Add new role',
      description: 'Create new role data',
      action: [
        authentication,
        authorization('api:add', ['role:add']),
        Handler.create
      ],
      validators: {
        headers: CommonSchema.Header.schema,
        body: Schema.body
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: Role',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
    {
      enabled: true,
      method: 'get',
      path: '/:_id',
      summary: 'Get an role',
      description: 'Get role by id',
      action: [
        authentication,
        authorization('api:read', ['role:read']),
        Handler.getOneBy
      ],
      validators: {
        headers: CommonSchema.Header.schema,
        // ? Why #RoleParams Schema didn't appear in Swagger UI
        params: Schema.params
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: Role',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
    {
      enabled: true,
      method: 'get',
      path: '/',
      summary: 'List of roles',
      description: 'Get multiple roles',
      action: [
        authentication,
        authorization('api:read', ['role:read']),
        Handler.getBy
      ],
      validators: {
        headers: CommonSchema.Header.schema,
        query: CommonSchema.Query.schema
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: Role',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
    {
      enabled: true,
      method: 'delete',
      path: '/:_id',
      summary: 'Delete an role',
      description: 'Delete an role by id',
      action: [
        authentication,
        authorization('api:remove', ['role:remove']),
        Handler.deleteOneBy
      ],
      validators: {
        headers: CommonSchema.Header.schema,
        // ? Why #RoleParams Schema didn't appear in Swagger UI
        params: Schema.params
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: Role',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
    {
      enabled: true,
      method: 'put',
      path: '/:_id',
      summary: 'Update an role',
      description: 'Update an role by id',
      action: [
        authentication,
        authorization('api:update', ['role:update']),
        Handler.updateOneBy
      ],
      validators: {
        headers: CommonSchema.Header.schema,
        // ? Why #RoleParams Schema didn't appear in Swagger UI
        params: Schema.params,
        body: Schema.body
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: Role',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
  ]
}

module.exports = definition;
