const { Endpoint: Handler } = require('../handler');
const { authentication, authorization } = require('../middleware');
const { Endpoint: Schema, Common: CommonSchema } = require('../schema')

const definition = {
  name: 'Endpoint',
  basePath: '/api/v1/endpoint',
  description: 'Endpoint APIs v1',
  routes: [
    {
      enabled: true,
      method: 'post',
      path: '/',
      summary: 'Add new endpoint',
      description: 'Create new endpoint data',
      action: [
        authentication,
        authorization('api:add', ['endpoint:add']),
        Handler.create
      ],
      validators: {
        headers: CommonSchema.Header.schema,
        body: Schema.body
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: Endpoint',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
    {
      enabled: true,
      method: 'get',
      path: '/:_id',
      summary: 'Get an endpoint',
      description: 'Get endpoint by id',
      action: [
        authentication,
        authorization('api:read', ['endpoint:read']),
        Handler.getOneBy
      ],
      validators: {
        headers: CommonSchema.Header.schema,
        // ? Why #EndpointParams Schema didn't appear in Swagger UI
        params: Schema.params
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: Endpoint',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
    {
      enabled: true,
      method: 'get',
      path: '/',
      summary: 'List of endpoints',
      description: 'Get multiple endpoints',
      action: [
        authentication,
        authorization('api:read', ['endpoint:read']),
        Handler.getBy
      ],
      validators: {
        headers: CommonSchema.Header.schema,
        query: CommonSchema.Query.schema
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: Endpoint',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
    {
      enabled: true,
      method: 'delete',
      path: '/:_id',
      summary: 'Delete an endpoint',
      description: 'Delete an endpoint by id',
      action: [
        authentication,
        authorization('api:remove', ['endpoint:remove']),
        Handler.deleteOneBy
      ],
      validators: {
        headers: CommonSchema.Header.schema,
        // ? Why #EndpointParams Schema didn't appear in Swagger UI
        params: Schema.params
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: Endpoint',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
    {
      enabled: true,
      method: 'put',
      path: '/:_id',
      summary: 'Update an endpoint',
      description: 'Update an endpoint by id',
      action: [
        authentication,
        authorization('api:update', ['endpoint:update']),
        Handler.updateOneBy
      ],
      validators: {
        headers: CommonSchema.Header.schema,
        // ? Why #EndpointParams Schema didn't appear in Swagger UI
        params: Schema.params,
        body: Schema.body
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: Endpoint',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
  ]
}

module.exports = definition;
