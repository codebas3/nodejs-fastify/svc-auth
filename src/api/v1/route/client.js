const { Client: Handler } = require('../handler');
const { authentication, authorization } = require('../middleware');
const { Client: Schema, Common: CommonSchema } = require('../schema')

const definition = {
  name: 'Client',
  basePath: '/api/v1/client',
  description: 'Client APIs v1',
  routes: [
    {
      enabled: true,
      method: 'post',
      path: '/',
      summary: 'Add new client',
      description: 'Create new client data',
      action: [
        authentication,
        authorization('api:add', ['client:add']),
        Handler.create
      ],
      validators: {
        headers: CommonSchema.Header.schema,
        body: Schema.body
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: Client',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
    {
      enabled: true,
      method: 'get',
      path: '/:_id',
      summary: 'Get a client',
      description: 'Get client by id',
      action: [
        authentication,
        authorization('api:read', ['client:read']),
        Handler.getOneBy
      ],
      validators: {
        headers: CommonSchema.Header.schema,
        // ? Why #ClientParams Schema didn't appear in Swagger UI
        params: Schema.params
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: Client',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
    {
      enabled: true,
      method: 'get',
      path: '/',
      summary: 'List of clients',
      description: 'Get multiple clients',
      action: [
        authentication,
        authorization('api:read', ['client:read']),
        Handler.getBy
      ],
      validators: {
        // headers: CommonSchema.Header.schema,
        query: CommonSchema.Query.schema
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: Client',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
    {
      enabled: true,
      method: 'delete',
      path: '/:_id',
      summary: 'Delete a client',
      description: 'Delete a client by id',
      action: [
        authentication,
        authorization('api:remove', ['client:remove']),
        Handler.deleteOneBy
      ],
      validators: {
        headers: CommonSchema.Header.schema,
        // ? Why #ClientParams Schema didn't appear in Swagger UI
        params: Schema.params
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: Client',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
    {
      enabled: true,
      method: 'put',
      path: '/:_id',
      summary: 'Update a client',
      description: 'Update a client by id',
      action: [
        authentication,
        authorization('api:update', ['client:update']),
        Handler.updateOneBy
      ],
      validators: {
        headers: CommonSchema.Header.schema,
        // ? Why #ClientParams Schema didn't appear in Swagger UI
        params: Schema.params,
        body: Schema.body
      },
      responseExamples: [
        {
          code: 200,
          description: 'OK: Client',
          mediaType: 'application/json',
          schema: Schema.response
        }
      ]
    },
  ]
}

module.exports = definition;
