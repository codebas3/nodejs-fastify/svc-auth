const { getReasonPhrase } = require('http-status-codes')
const joi = require('joi');
const { app: { codename } } = require('../../../config')

const row = {
  field: joi.string().required().example('scope'),
  value: joi.array().required().items(joi.object().keys({
    type: joi.array().example('mandatory'),
    data: joi.array().items(joi.string().example('openid'))
  }))
};

const response = joi.object().id('ScopeResponse').keys({
  statusCode: joi.number().required().allow(200).example(200),
  error: joi.string().required().allow(getReasonPhrase(200)).example(getReasonPhrase(200)),
  data: joi.object().keys({
    count: joi.number().required().min(0).example(99),
    rows: joi.array().min(0).required().allow(null)
      .items(row)
  }),
  _: joi.array().min(0).required().allow(null)
    .items(joi.link('#Underscore'))
})

module.exports = {
  row,
  response
}
