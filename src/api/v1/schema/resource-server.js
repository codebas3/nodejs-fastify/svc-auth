const { getReasonPhrase } = require('http-status-codes')
const joi = require('joi');
const { app: { codename } } = require('../../../config')

const _id = joi.string().required()

const base = {
  code: joi.string().required().example('svc-user'),
  name: joi.string().required().example('User service'),
  uri: joi.string().required().example('https://user.api.baba-pte.com'),
  isPublic: joi.bool().required().example(true),
  scopes: joi.array().min(0).required().allow(null)
    .items(joi.string().required().example('svc-user:api:read')),
  notes: joi.string().allow(null, '').example('your note goes here..')
};

const body = joi.object().id('ResourceServerBody').keys({ ...base })

const row = joi.object().id('ResourceServerRow').keys({ _id, ...base })

const params = joi.object().id('ResourceServerParams').keys({ _id })

const response = joi.object().id('ResourceServerResponse').keys({
  statusCode: joi.number().required().allow(200).example(200),
  error: joi.string().required().allow(getReasonPhrase(200)).example(getReasonPhrase(200)),
  data: joi.object().keys({
    count: joi.number().required().min(0).example(99),
    rows: joi.array().min(0).required().allow(null)
      .items(row)
  }),
  _: joi.array().min(0).required().allow(null)
    .items(joi.link('#Underscore')),
})

module.exports = {
  body,
  row,
  params,
  response
}
