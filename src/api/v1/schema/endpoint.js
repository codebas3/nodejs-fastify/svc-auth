const { getReasonPhrase } = require('http-status-codes')
const joi = require('joi');

const _id = joi.string().required()

const base = {
  code: joi.string().required().example('client:create'),
  name: joi.string().required().example('Create client credential'),
  isInternalDefault: joi.bool().required().example(true),
  isExternalDefault: joi.bool().required().example(false),
  resourceServerId: joi.string().allow(null).example('646cb6e5c96c13a457779354'),
  roleIds: joi.array().allow(null)
    .items(joi.string().required().example('646945103f16af210dc50899')),
  notes: joi.string().allow(null, '').example('your note goes here..')
};

const body = joi.object().id('EndpointBody').keys({ ...base })

const row = joi.object().id('EndpointRow').keys({ _id, ...base })

const params = joi.object().id('EndpointParams').keys({ _id })

const response = joi.object().id('EndpointResponse').keys({
  statusCode: joi.number().required().allow(200).example(200),
  error: joi.string().required().allow(getReasonPhrase(200)).example(getReasonPhrase(200)),
  data: joi.object().keys({
    count: joi.number().required().min(0).example(99),
    rows: joi.array().min(0).required().allow(null)
      .items(row)
  }),
  _: joi.array().min(0).required().allow(null)
    .items(joi.link('#Underscore')),
})

module.exports = {
  body,
  row,
  params,
  response
}
