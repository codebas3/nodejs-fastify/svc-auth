const { getReasonPhrase } = require('http-status-codes')
const joi = require('joi');

const _id = joi.string().required()

const base = {
  code: joi.string().required().example('superadmin'),
  name: joi.string().required().example('Super Admin'),
  isInternalDefault: joi.bool().required().example(false),
  isExternalDefault: joi.bool().required().example(false),
  notes: joi.string().allow(null, '').example('your note goes here..')
};

const body = joi.object().id('RoleBody').keys({ ...base })

const row = joi.object().id('RoleRow').keys({ _id, ...base })

const params = joi.object().id('RoleParams').keys({ _id })

const response = joi.object().id('RoleResponse').keys({
  statusCode: joi.number().required().allow(200).example(200),
  error: joi.string().required().allow(getReasonPhrase(200)).example(getReasonPhrase(200)),
  data: joi.object().keys({
    count: joi.number().required().min(0).example(99),
    rows: joi.array().min(0).required().allow(null)
      .items(row)
  }),
  _: joi.array().min(0).required().allow(null)
    .items(joi.link('#Underscore')),
})

module.exports = {
  body,
  row,
  params,
  response
}
