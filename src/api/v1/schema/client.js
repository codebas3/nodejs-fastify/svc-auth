const { getReasonPhrase } = require('http-status-codes')
const joi = require('joi');
const { app: { codename } } = require('../../../config')

const _id = joi.string().required()

const base = {
  payload: joi.object().keys({
    client_id: joi.string().required().example('yowza-fooey-gah'),
    client_secret: joi.string().required().example('UNnQqTUrLUmZKH3zar8YdCjB'),
    client_name: joi.string().required().example('The Baba Company'),
    redirect_uris: joi.array().min(0).required().allow(null)
      .items(joi.string().required().example('https://baba-pte.com/callback')),
    grant_types: joi.array().min(0).required().allow(null)
      .items(joi.string().required().example('authorization_code')),
    scope: joi.string().required().example('openid offline_access profile'),
    resource_scope: joi.string().required().example('svc-chat:read svc-food:read'),
    is_internal: joi.bool().required().example(true)
  }),
  notes: joi.string().allow(null, '').example('your note goes here..')
};

const body = joi.object().id('ClientBody').keys({ ...base })

const row = joi.object().id('ClientRow').keys({ _id, ...base })

const params = joi.object().id('ClientParams').keys({ _id })

const response = joi.object().id('ClientResponse').keys({
  statusCode: joi.number().required().allow(200).example(200),
  error: joi.string().required().allow(getReasonPhrase(200)).example(getReasonPhrase(200)),
  data: joi.object().keys({
    count: joi.number().required().min(0).example(99),
    rows: joi.array().min(0).required().allow(null)
      .items(row)
  }),
  _: joi.array().min(0).required().allow(null)
    .items(joi.link('#Underscore')),
})

module.exports = {
  body,
  row,
  params,
  response
}
