const { getReasonPhrase } = require('http-status-codes')
const joi = require('joi');
const { app: { codename } } = require('../../../config')

const base = {
  name: joi.string().required().example('maria'),
  status: joi.string().required().example('up'),
  notes: joi.string().allow(null, '').example('your note goes here..')
};

const response = joi.object().id('HealthCheckResponse').keys({
  statusCode: joi.number().required().allow(200).example(200),
  error: joi.string().required().allow(getReasonPhrase(200)).example(getReasonPhrase(200)),
  data: joi.array().min(0).required().allow(null)
    .items(joi.object().keys({ ...base })),
  _: joi.array().min(0).required().allow(null)
    .items(joi.link('#Underscore')),
})

module.exports = { response }
