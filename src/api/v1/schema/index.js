module.exports = {
  Common: require('./common'),
  HealthCheck: require('./health-check'),
  Client: require('./client'),
  Endpoint: require('./endpoint'),
  Scope: require('./scope'),
  ResourceServer: require('./resource-server'),
  Role: require('./role')
}
