const _ = require('lodash')
const joi = require('joi')
const { ReasonPhrases, StatusCodes } = require('http-status-codes')
const { codename } = require('../../../config/app')

module.exports = async (req) => {
  const { routeSchema = {} } = req
  const exceptions = [];

  let i = 0;
  for (const httpPart in routeSchema) {
    const joiSchema = routeSchema[httpPart];

    if (!joi.isSchema(joiSchema)) continue;

    const { error: e } = joiSchema.validate(
      req[httpPart],
      { abortEarly: false, allowUnknown: httpPart === 'headers' }
    )
    if (e) {
      const HttpPart = _.capitalize(httpPart);
      for (const d in e.details) {
        const { message } = e.details[d];
        exceptions.push({
          code: `${HttpPart}Validation${i}`,
          message: `${HttpPart}: ${message}`
        })

        i += 1
      }
    }

    // This is wrong way to validate one by one field!
    // let j = 0;
    // for (let [field, { id, schema }] of joiSchema._ids._byKey) {
    //   const { error } = schema.validate(req[httpPart][field])
    //   if (error) {
    //     for (let d in error.details) {
    //       const message = error.details[d].message.replace(/\"/g, '');
    //       exceptions.push({
    //         code: `${_.capitalize(httpPart)}Validation${j++}`,
    //         message: `${id} ${message}`
    //       })
    //     }
    //   }
    // }
  }

  if (exceptions.length) {
    const error = new Error('Incorrect input data parsing')
    error.code = `${codename}Parse`
    error.statusCode = StatusCodes.BAD_REQUEST;
    error.error = ReasonPhrases.BAD_REQUEST;
    error[`${codename}`] = exceptions

    throw error
  }
}
