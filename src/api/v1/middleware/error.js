const _ = require('lodash');
const { ReasonPhrases, StatusCodes } = require('http-status-codes')

const { app, constants: { TYPE_ERROR, TYPE_WARN } } = require('../../../config');
const logger = require('../../../logger/console')(__filename);
const { stringify, objError } = require('../../../logger/lib/format');

const errorCodeFinder = (err) => {
  const anEmpty = [undefined, null, 0, ''];
  return _.without([err.code, err.no, err.errno], ...anEmpty).join(' ') || 'UNDEFINED'
}

const buildErrors = (err) => {
  logger.error(stringify(TYPE_ERROR.UNCAUGHT, objError(err)));

  if (err.parent) {
    if (err.parent.message !== err.message) {
      logger.error(stringify(TYPE_ERROR.UNCAUGHT, objError(err.parent)))
    }
  }
  if (err.original) {
    if (err.parent) {
      if (err.original.message !== err.parent.message) {
        logger.error(stringify(TYPE_ERROR.UNCAUGHT, objError(err.original)))
      }
    }
  }

  // * Build stacked error messages
  const messages = [
    { code: errorCodeFinder(err), message: err.message }
  ];
  if (err.parent) {
    if (err.parent.message !== err.message) {
      messages.push({ code: errorCodeFinder(err.parent), message: err.parent.message });
    }
  }
  if (err.original) {
    if (err.parent) {
      if (err.original.message !== err.parent.message) {
        messages.push({ code: errorCodeFinder(err.original), message: err.original.message });
      }
    }
  }

  return messages
}

/* Catch any exceptions / error */
module.exports = function (exceptions, _req, res) {
  try {
    const {
      statusCode, error, code, message
    } = exceptions;

    if (exceptions[app.codename]) {
      logger.warn(stringify(TYPE_WARN.SERVICE_V1, objError(exceptions)))

      return res.code(statusCode).send({
        statusCode,
        error,
        message: `${code}: ${message}`,
        _: exceptions[app.codename]
      })
    }

    let data = [];

    if (exceptions instanceof Array) {
      exceptions.forEach((_err) => {
        data = data.concat(buildErrors(_err))
      })
    } else {
      data = data.concat(buildErrors(exceptions))
    }

    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send({
      statusCode: StatusCodes.INTERNAL_SERVER_ERROR,
      error: ReasonPhrases.INTERNAL_SERVER_ERROR,
      message: app.codename,
      _: data
    })
  } catch (e) {
    logger.error(stringify(TYPE_ERROR.UNCAUGHT, objError(e)))
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(e)
  }
};
