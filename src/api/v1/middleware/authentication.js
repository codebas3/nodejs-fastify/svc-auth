/* eslint-disable no-ex-assign */

const _ = require('lodash');
const { ReasonPhrases, StatusCodes } = require('http-status-codes')

const { hosts, constants: { TYPE_ERROR }, app } = require('../../../config');
const { decode } = require('../../../util/token');
const logger = require('../../../logger/console')(__filename);
const { stringify, objError, newError } = require('../../../logger/lib/format');
const Manifest = require('../../../manifest');

module.exports = async (req, res) => {
  const { redis } = Manifest.getInstance();
  const authorization = req.headers.authorization || '';
  const tokens = authorization.split(/\s/g)
  const token = tokens[tokens.length === 1 ? 0 : 1]

  try {
    const decoded = await decode(token);

    // just make sure this system use redis
    if (redis) {
      // FIXME: It's should be use access_token!
      const isLoggedOut = await redis.client.get(`del:id_token:${token}`);
      if (isLoggedOut) throw newError('invalidSession', req.language)
    }

    if (hosts.svc.auth !== decoded.iss) throw newError('invalidIssuer', req.language)

    const { profile, roles } = decoded
    if (!profile.id) throw newError('invalidIdentity', req.language)

    req.usr = { ...profile, roles: _.uniq(roles) };
  } catch (err) {
    if (err[app.codename] || err.message === 'jwt expired' || err.message === 'invalid token') {
      logger.error(stringify(TYPE_ERROR.AUTHENTICATION, objError(err)))

      if (err.message === 'invalid token') err = newError('invalidSessionFormat', req.language)
      if (err.message === 'jwt expired') err = newError('invalidSessionExp', req.language)

      res.status(StatusCodes.UNAUTHORIZED).send({
        statusCode: StatusCodes.UNAUTHORIZED,
        error: ReasonPhrases.UNAUTHORIZED,
        messages: `${err.code}: ${err.message}`,
        _: err[app.codename]
      })
    } else {
      throw err
    }
  }
};
