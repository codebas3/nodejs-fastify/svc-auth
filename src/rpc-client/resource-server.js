const Promise = require('bluebird');
const config = require('../config');
const logger = require('../logger/rpc-client')(__filename);
const { stringify } = require('../logger/lib/format');
const grpcClient = require('../util/grpc-client');

const { grpc: { serviceOpts } } = config;
const { TYPE_INFO } = config.constants;

module.exports = class ResourceServerGRPCClient {
  constructor() {
    this.module = {
      name: 'ResourceServer', // Package name inside .proto
      service: 'ResourceServerMethod', // Service name inside .proto
      proto: `${__dirname}/../rpc/v1/proto/resource-server.proto`,
      host: config.hosts.rpc.auth
    }

    logger.info(stringify(TYPE_INFO.GRPC_CLIENT, {
      title: 'Load gRPC service',
      data: this.module
    }))

    this.method = Promise.promisifyAll(grpcClient(this.module, serviceOpts));
  }

  async getAll() {
    const response = await this.method.GetResourceServersAsync(null)

    logger.info(stringify(TYPE_INFO.GRPC_CLIENT, {
      title: 'ResourceServerMethod.GetResourceServer',
      data: response
    }))

    return response
  }
}
