const Promise = require('bluebird');
const config = require('../config');
const logger = require('../logger/rpc-client')(__filename);
const { stringify } = require('../logger/lib/format');
const grpcClient = require('../util/grpc-client');

const { grpc: { serviceOpts } } = config;
const { TYPE_INFO } = config.constants;

module.exports = class RoleGRPCClient {
  constructor() {
    this.module = {
      name: 'Role', // Package name inside .proto
      service: 'RoleMethod', // Service name inside .proto
      proto: `${__dirname}/../rpc/v1/proto/role.proto`,
      host: config.hosts.rpc.auth
    }

    logger.info(stringify(TYPE_INFO.GRPC_CLIENT, {
      title: 'Load gRPC service',
      data: this.module
    }))

    this.method = Promise.promisifyAll(grpcClient(this.module, serviceOpts));
  }

  async getAll() {
    const response = await this.method.GetRolesAsync(null)

    logger.info(stringify(TYPE_INFO.GRPC_CLIENT, {
      title: 'RoleMethod.GetRole',
      data: response
    }))

    return response
  }
}
