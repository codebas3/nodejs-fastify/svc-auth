const fs = require('fs')
const path = require('path')
const { faker } = require('@faker-js/faker');

const logger = require('./logger/console')(__filename);
const { stringify } = require('./logger/lib/format');
const {
  env, constants, app, hosts
} = require('./config');
const Singleton = require('./util/singleton');
const {
  ResourceServer, Client, Role, Endpoint
} = require('./queue-consumer/rabbitmq');

const { TYPE_ERROR, MESSAGE } = constants;

class Manifest extends Singleton {
  async setup() {
    await this.setRedis()
    await this.setMongoDB()
    await this.setRabbitMQ()
    await this.subscribeQueue()
    await this.setServer()
    await this.setGRPC()
    // await this.setCron()

    return this;
  }

  async stop() {
    await this.redis.disconnect();
    await this.mongo.disconnect();
    await this.rmq.disconnect();
    await this.grpc.stop(true);
    await this.server.stop();
    // await this.cron.stopAll();
  }

  async start() {
    if (env !== 'test') await this.server.start();
    await this.grpc.start();
    await this.dataInitialize();
    // this.cron.jobTest();
  }

  async setRedis() {
    const { Redis } = require('./cache');
    this.redis = Redis.getInstance();
    await this.redis.connect();
  }

  async setMongoDB() {
    const Mongo = require('./model/mongo');
    const MongoRepo = require('./repository/mongo');
    this.mongo = Mongo.getInstance();
    await this.mongo.connect();
    this.mongo.repo = MongoRepo;
  }

  async setRabbitMQ() {
    const { Client: RMQ } = require('./queue/rabbitmq');
    const me = this;

    const channelId = `ch:${app.name}:${faker.word.adjective()}`

    // * reusable client
    me.rmq = RMQ.getInstance()

    // * reusable channel
    me.rmq.channel = await this.rmq.confirmChannel(channelId);

    me.rmq.channel.listeners('return').map(fn => me.rmq.channel.removeListener('return', fn))
    me.rmq.channel.on('return', ({ fields, content }) => {
      const data = { fields, content: JSON.parse(content) }
      logger.error(
        stringify(
          TYPE_ERROR.RABBIT_MQ,
          `${MESSAGE.RABBIT_MQ_PUBLISH_FALLBACK} (${me.rmq.channel._id}): Failed to publish`,
          data
        )
      )
    })
  }

  async subscribeQueue() {
    // Data subscriptions
    ResourceServer.subscribe(this.rmq)
    Role.subscribe(this.rmq) // this is should on svc-user
    Endpoint.subscribe(this.rmq)
  }

  // eslint-disable-next-line class-methods-use-this
  async dataInitialize() {
    // Data initializer
    await ResourceServer.sync(true);
    await Client.sync(true);
    await Role.sync(true);
    await Endpoint.sync(true);
  }

  async setServer() {
    const Server = require('./server');
    const allowedOrigins = ['*'];
    const corsOptions = {
      origin(origin, callback) {
        if (!origin) {
          /* Allow requests with no origin (like mobile apps or curl requests) */
          return callback(null, true);
        }

        if (allowedOrigins.includes('*')) return callback(null, true);
        if (!allowedOrigins.includes(origin)) {
          const message = 'The CORS policy for this site does not allow access from the specified origin.';
          return callback(new Error(message), false);
        }

        return callback(null, true);
      },
      optionsSuccessStatus: 200
    };

    const assetdir = path.resolve(__dirname, '..', 'dist', 'asset');
    if (!fs.existsSync(assetdir)) fs.mkdirSync(assetdir, { recursive: true });

    const pagedir = path.resolve(__dirname, 'page');
    const webmanifest = JSON.parse(fs.readFileSync(
      path.join(pagedir, 'asset', 'site.webmanifest')
    ));
    fs.writeFileSync(
      path.resolve(assetdir, 'site.webmanifest'),
      JSON.stringify({
        ...webmanifest,
        name: `${app.project}: ${app.codename}`,
        short_name: app.project
      }, 0, 2)
    )

    this.server = Server.getInstance();
    await this.server.setApp({ docs: true, corsOptions })

    return this.server;
  }

  async setGRPC() {
    const GRPC = require('./rpc/grpc');
    this.grpc = GRPC.getInstance();
  }

  async setCron() {
    const Cron = require('./cron');
    this.cron = Cron.getInstance();
  }
}

module.exports = Manifest;
