const fs = require('fs');
const path = require('path');
const Fastify = require('fastify')
const qs = require('qs')
const ejs = require('ejs')
const Http = require('http')
const { convert } = require('joi-route-to-swagger')
const _middie = require('@fastify/middie')
const _view = require('@fastify/view')
const _helmet = require('@fastify/helmet');
const _cors = require('@fastify/cors');
const _static = require('@fastify/static');
const _swagger = require('@fastify/swagger');
const _swaggerUI = require('@fastify/swagger-ui');
const _cookie = require('@fastify/cookie');
const _formBody = require('@fastify/formbody'); // for application/x-www-form-urlencoded content type

const Singleton = require('./util/singleton');
const logger = require('./logger/console')(__filename);
const { stringify } = require('./logger/lib/format');

// Page definition
const PageRoute = require('./page/route');
const PageMiddleware = require('./page/middleware');

// APIv1 definition
const ApiV1Route = require('./api/v1/route');
const ApiV1Middleware = require('./api/v1/middleware');

// Oidc definition
const Oidc = require('./openid/oidc');
const OidcRoute = require('./openid/route');
const OidcMiddleware = require('./openid/middleware');

const {
  env, app, constants, swaggerUI, hosts, auth,
} = require('./config');

const { TYPE_INFO, MESSAGE } = constants;

class Server extends Singleton {
  opts;

  http;

  app;

  basePath;

  oidc = Oidc.getInstance();

  constructor() {
    super();

    const me = this;

    me.basePath = hosts.svc.auth.replace(/http(s?):\/\//g, '').split('/').slice(1).join('/')
    me.basePath = me.basePath ? `/${me.basePath}` : ''

    me.app = Fastify({
      logger: false,
      serverFactory: (handler) => {
        me.http = Http.createServer((req, res) => {
          handler(req, res)
        });

        return me.http
      },
      querystringParser: str => qs.parse(str),
      // ajv: {
      //   customOptions: {
      //     keywords: ['collectionFormat']
      //   }
      // }
    });
  }

  /**
   * Fastify configuration
   * @param {*} option - required parameters to run the app
   */
  async setApp(option) {
    const me = this;

    me.opts = option

    // First we need fastify compatible to middleware
    await me.app.register(_middie)

    // View engine
    me.app.register(_view, {
      engine: { ejs },
      includeViewExtension: true,
      root: path.resolve(__dirname, 'page', 'view')
    });

    // Static path
    me.app.register(_static, {
      root: path.join(__dirname, '..', 'dist')
    })

    // Cookie support
    me.app.register(_cookie, {
      // secret: app.cookies,
      hook: 'onRequest', // set to false to disable cookie autoparsing or set autoparsing on any of the following hooks: 'onRequest', 'preParsing', 'preHandler', 'preValidation'. default: 'onRequest'
      parseOptions: {} // options for parsing cookies
    })

    // OpenAPI
    if (me.opts.docs) {
      Object.values(ApiV1Route).forEach(el => {
        el.routes = el.routes.filter(route => route.enabled !== false)
      })

      const { DOCS_DEF, ROUTE_DEF } = require('./api/docs');
      const openAPI = convert(
        Object.values(ApiV1Route),
        DOCS_DEF,
        ROUTE_DEF
      )

      const distdir = path.resolve(`${__dirname}/../dist`);
      const jsonOpenAPI = path.join(distdir, 'swagger.json');
      // Fix swagger trailing path
      Object.keys(openAPI.paths).forEach(url => {
        if (url.slice(-1) === '/') {
          openAPI.paths[url.slice(0, -1)] = openAPI.paths[url];
          delete openAPI.paths[url];
        }
      })
      if (!fs.existsSync(distdir)) fs.mkdirSync(distdir);
      fs.writeFileSync(jsonOpenAPI, JSON.stringify(openAPI, 0, 2))

      me.app.register(_swagger, {
        mode: 'static',
        specification: { path: jsonOpenAPI },
        exposeRoute: false
      })
      me.app.register(_swaggerUI, swaggerUI)
    }

    // Cors
    if (me.opts.corsOptions) me.app.register(_cors, me.opts.corsOptions);

    // Content Security Policy
    const directives = _helmet.contentSecurityPolicy.getDefaultDirectives();
    delete directives['form-action'];
    if (env !== 'production') {
      delete directives['script-src'];
      delete directives['style-src'];
    } else {
      me.app.register(_helmet, {
        contentSecurityPolicy: {
          useDefaults: false,
          directives: {
            ...directives,
            'script-src': ["'self'", "'unsafe-inline'", 'https://unpkg.com', 'https://cdnjs.cloudflare.com'],
            'style-src': ["'self'", "'unsafe-inline'", 'https://unpkg.com'],
          },
        },
      });
    }
    me.directives = directives;

    // User language
    me.app.use(ApiV1Middleware.language);

    // Format validator
    me.app.setValidatorCompiler(_req => _data => true) // Just skip built-in validator

    // Page routes
    const PagePrefix = '/';
    me.app.register((Router, _opts, done) => {
      const { routes } = PageRoute
      const prefix = PageRoute.basePath.replace(PagePrefix, '')

      Router.register(_formBody);
      Router.addHook('preValidation', PageMiddleware.cookieAuthentication);

      routes.forEach((r) => {
        if (!r.enabled) return;

        let url = (prefix + r.path);
        url = url.slice(-1) === '/' ? url.slice(0, -1) : url;

        Router.route({
          url: r.path,
          method: r.method.toUpperCase(),
          preValidation: [
            ...r.action.slice(0, -1)
          ],
          handler: r.action.slice(-1)[0],
        })
      })

      done()
    }, { prefix: PagePrefix })

    // Oidc instance & it's routes
    const { provider } = await me.oidc.start()
    me.app.use(OidcRoute.basePath, provider.callback())
    me.app.register((Router, _opts, done) => {
      const { routes, basePath } = OidcRoute

      Router.register(_formBody);
      Router.use(OidcMiddleware.noCache);

      routes.forEach((r) => {
        if (!r.enabled) return;

        Router.route({
          url: r.path,
          method: r.method.toUpperCase(),
          preValidation: [
            ...r.action.slice(0, -1)
          ],
          handler: r.action.slice(-1)[0],
        })
      })

      done()
    }, { prefix: OidcRoute.basePath })

    // Api v1 routes
    const ApiV1Prefix = '/api/v1';
    me.app.register((Router, _opts, done) => {
      Router.use(ApiV1Middleware.tracker.binder) // log binder
      Router.addHook('preValidation', ApiV1Middleware.tracker.logging.request) // log request
      Router.addHook('onResponse', ApiV1Middleware.tracker.logging.response) // log request

      Object.values(ApiV1Route).forEach(definition => {
        const prefix = definition.basePath.replace(ApiV1Prefix, '')
        definition.routes.forEach(r => {
          if (!r.enabled) return;

          let url = (prefix + r.path);
          url = url.slice(-1) === '/' ? url.slice(0, -1) : url;

          // logger.info(`Registering: ${route.method} ${route.url}`)
          Router.route({
            url,
            method: r.method.toUpperCase(),
            preValidation: [
              ApiV1Middleware.validator,
              ...r.action.slice(0, -1)
            ],
            handler: r.action.slice(-1)[0],
            prefixTrailingSlash: 'both', // doesn't affected?!
            schema: { ...(r.validators || {}) },

            /**
             * This function will validate on each request and triggered onParsing fastify hook.
             * but we can't reply user with multiple joi validation errors messages,
             * in case you have header/query/params/body schema, and user did request with invalid input schemas.
             * So, let's move this validation to preValidation hook.
             * @param {*} param0 req
             * @returns
             */
            // validatorCompiler: ({ schema, method, url, httpPart } = req) => {
            //   return (data) => true
            // }
          })
        })
      })

      done()
    }, { prefix: ApiV1Prefix })

    // Default error handler
    me.app.setErrorHandler(ApiV1Middleware.error)

    await me.app.ready();

    return me;
  }

  async start() {
    this.app.listen({ port: app.port }, () => logger.info(stringify(
      TYPE_INFO.SYSTEM,
      `${MESSAGE.HTTP_LISTENED} ${app.port}...`
    )))
  }

  async stop() {
    return this.app.close(() => logger.info(stringify(
      TYPE_INFO.SYSTEM,
      `${MESSAGE.HTTP_STOPPED} ${app.port}`
    )))
  }
}

module.exports = Server;
