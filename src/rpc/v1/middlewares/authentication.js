const { TYPE_INFO, TYPE_WARN } = require('../../../config/constants');
const logger = require('../../../logger/rpc')(__filename);
// const { stringify, objError } = require('../../../logger/lib/format');

module.exports = async (ctx, next) => {
  // FIXME: Make sure process auth middleware before next() by validate ctx.metadata

  // * Delete this logger.warn if you already implement auth processing
  logger.warn(JSON.stringify({
    type: TYPE_WARN.GRPC,
    message: 'Auth middleware is must!',
    fullname: ctx.fullName,
    metadata: ctx.metadata
  }, 0, 2))

  await next();
};
