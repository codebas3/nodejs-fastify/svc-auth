const logger = require('../../../../logger')

module.exports = (ctx, next) => {
  const correlation = logger.lib.correlation('http');
  correlation.bindEmitter(ctx.req);
  correlation.bindEmitter(ctx.res);
  correlation.withId(() => {
    const thisId = correlation.getId();
    ctx.res.set('x-correlation-id', thisId);
    next();
  }, ctx.req.get('x-correlation-id'));
};
