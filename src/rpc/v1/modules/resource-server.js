/* eslint-disable object-curly-newline */

const protoName = 'ResourceServer';
const protoService = 'ResourceServerMethod';

const proto = require('path').resolve(`${__dirname}/../proto/resource-server.proto`);
const { ResourceServer: Service } = require('../../../service/v1');

const GetResourceServers = async (ctx, next) => {
  const service = new Service()
  const { rows } = await service.getBy({
    count: false,
    limit: Infinity
  });

  ctx.res = { rows };
};

module.exports = {
  name: protoName,
  service: protoService,
  proto,
  handlers: [
    { method: GetResourceServers }
  ]
};
