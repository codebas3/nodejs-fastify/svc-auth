/* eslint-disable object-curly-newline */

const protoName = 'Endpoint';
const protoService = 'EndpointMethod';

const proto = require('path').resolve(`${__dirname}/../proto/endpoint.proto`);
const { Endpoint: Service } = require('../../../service/v1');

const GetEndpoints = async (ctx, next) => {
  const service = new Service()
  const { rows } = await service.getBy({
    count: false,
    limit: Infinity
  });

  ctx.res = { rows };
};

module.exports = {
  name: protoName,
  service: protoService,
  proto,
  handlers: [
    { method: GetEndpoints }
  ]
};
