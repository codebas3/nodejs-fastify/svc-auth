/* eslint-disable object-curly-newline */

const protoName = 'Role';
const protoService = 'RoleMethod';

const proto = require('path').resolve(`${__dirname}/../proto/role.proto`);
const { Role: Service } = require('../../../service/v1');

const GetRoles = async (ctx, next) => {
  const service = new Service()
  const { rows } = await service.getBy({
    count: false,
    limit: Infinity
  });

  ctx.res = { rows };
};

module.exports = {
  name: protoName,
  service: protoService,
  proto,
  handlers: [
    { method: GetRoles }
  ]
};
