module.exports = {
  Ping: require('./ping'),
  ResourceServer: require('./resource-server'),
  Role: require('./role'),
  Endpoint: require('./endpoint'),
}
