const os = require('os');

const Chamber = require('../../chamber')
const { Subscriber } = require('../../queue/rabbitmq');
const { Client: Service } = require('../../service/v1');
const { console: consoleLogger, lib: { correlationCache } } = require('../../logger');
const { stringify, objError } = require('../../logger/lib/format');
const { rabbitmq: opts, constants, env } = require('../../config');

const logger = consoleLogger(__filename)
const correlation = correlationCache('queue');

const { TYPE_ERROR, TYPE_INFO } = constants;

const sync = async (exit) => {
  const service = new Service();
  try {
    const clients = await service.getBy({
      count: false,
      limit: Infinity
    });
    const chamber = Chamber.getInstance();

    chamber.sync('client', clients.rows)

    return clients;
  } catch (err) {
    if (exit) throw err
    else {
      logger.error(stringify(TYPE_ERROR.GRPC_CLIENT, objError(err)));
      return undefined
    }
  }
}

const subscribe = async (client, callback) => {
  if (env === 'test') {
    return callback instanceof Function ? callback() : null
  }

  const { Client: obj } = opts.queue;
  const queueName = `${obj.queue}:${os.hostname()}`
  const subscriber = new Subscriber(client, obj.exchange, client.channel)

  return subscriber.consume(queueName, obj.route, null, async (msg) => {
    logger.info(stringify(
      `${TYPE_INFO.RABBIT_MQ}`,
      `Consuming ${queueName}: ${msg.content.toString()}`
    ));
    correlation.storeId();

    const rows = await sync();
    return callback instanceof Function ? callback(rows) : rows
  }, -1);
}

module.exports = {
  sync,
  subscribe
}
