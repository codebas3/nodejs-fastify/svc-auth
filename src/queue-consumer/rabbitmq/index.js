module.exports = {
  ResourceServer: require('./resource-server'),
  Client: require('./client'),
  Role: require('./role'),
  Endpoint: require('./endpoint')
}
