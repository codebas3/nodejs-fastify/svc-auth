const Promise = require('bluebird');
const { CronJob } = require('cron');

const { cron: opts } = require('../config');
const { Singleton } = require('../util');
const { cron: cronLogger, lib: { correlationCache } } = require('../logger');

const logger = cronLogger(__filename)
const correlation = correlationCache('cron');

class Cron extends Singleton {
  defaultTime;

  constructor() {
    super();
    this.defaultTime = opts.cronTime;
  }

  async stop(key) {
    if (this.job[key]?.running) {
      logger.info(`Job "${key}" stopped!`);
      return this.job[key].stop();
    }

    return undefined;
  }

  async stopAll() {
    const me = this;
    return Promise.map(Object.keys(this.job), (k) => me.stop(k))
  }

  #create(key, time, onTick, onComplete = null) {
    this.job = this.job || {};
    this.job[key] = new CronJob(time || this.defaultTime, onTick, onComplete);

    if (!this.job[key].running) this.job[key].start();
  }

  jobTest(onComplete) {
    const key = 'test';
    // this.#create(key, '0 */5 * * * *', () => {
    this.#create(key, '* * * * * *', () => {
      correlation.storeId();
      logger.info(`Job "${key}" spawned!`);
    }, onComplete)
  }
}

module.exports = Cron;
