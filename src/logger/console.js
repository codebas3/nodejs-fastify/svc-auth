const { app: { name, logLevel } } = require('../config');
const { logger, stack, correlation } = require('./lib');

module.exports = (filename) => logger({
  name,
  level: logLevel,
  correlation: correlation('console'),
  caller: (filename || stack(3)) || undefined
})
