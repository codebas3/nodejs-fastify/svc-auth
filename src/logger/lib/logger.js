const Pino = require('pino');

const { env, app: { name } } = require('../../config');

const storage = {};

module.exports = function (opts = {}) {
  const {
    level = 'info',
    correlation,
    noCorrelationIdValue = 'nocorrelation',
    caller
  } = opts;

  if (storage[correlation.namespace]) return storage[correlation.namespace]

  const pinoOpts = {
    level,
    formatters: {
      log(object) {
        Object.assign(object, {
          service: name,
          namespace: correlation.namespace,
          correlationId: correlation.getId() || noCorrelationIdValue,
          caller: caller || 'console'
        })

        return object
      },
      level(label) {
        return { level: label.toUpperCase() };
      },
    },
    timestamp: Pino.stdTimeFunctions.isoTime,
  };

  if (env === 'development') {
    pinoOpts.transport = {
      target: 'pino-pretty',
      options: {
        sync: true,
        colorize: true,
        colorizeObjects: true
      }
    }
  }

  storage[correlation.namespace] = Pino(pinoOpts);

  return storage[correlation.namespace];
};
