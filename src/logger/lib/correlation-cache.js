const uuid = require('uuid');
const Cache = require('node-cache');

const storage = {};

module.exports = (namespace = 'cache') => {
  if (storage[namespace]) return storage[namespace]

  const caching = new Cache();

  function storeId() {
    const store = caching.set(namespace, uuid.v4());
    return store;
  }

  function getId() {
    return caching.get(namespace);
  }

  storage[namespace] = {
    namespace,
    storeId,
    getId,
  }

  return storage[namespace];
}
