const { app, message: Err } = require('../../config');
const Slimy = require('../../util/slimy');
const { codename } = require('../../config/app')

const slimy = Slimy.getInstance();
// slimy.SEPARATOR = '[REDUCTED]';

const stringify = (type, message, reference) => ({
  msg: {
    type,
    reference,
    message: slimy.reduction(message)
  }
});

const objError = (error) => {
  const {
    code,
    message,
    stack
  } = error;

  return {
    code,
    reason: (message || '').split('\n').length > 1 ? message.split('\n') : message,
    stack: (stack || '').split('\n').length > 1 ? stack.split('\n') : stack,
    _: error[app.codename]
  }
};

const newError = (key, lang, params) => {
  const { message, code } = Err.get(key, lang, params)

  const error = new Error('Incorrect input data validation')
  error.code = `${codename}Validate`
  error[`${codename}`] = { message, code }

  return error
};

module.exports = {
  stringify,
  objError,
  newError
};
