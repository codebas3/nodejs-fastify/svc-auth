const { app: { name, logLevel } } = require('../config');
const { logger, stack, correlationCache } = require('./lib');

module.exports = (filename) => logger({
  name,
  level: logLevel,
  correlation: correlationCache('rpc-client'),
  caller: (filename || stack(3)) || undefined
})
