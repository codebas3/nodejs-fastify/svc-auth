const { validator } = require('./resource_server')

const enumeration = {
  scopes: ['openid', 'offline_access', 'profile'],
  grantTypes: [
    'authorization_code', 'password',
    'refresh_token', 'client_credentials',
    'implicit', 'urn:ietf:params:oauth:grant-type:device_code'
  ]
};

const setId = function () {
  this._id = this.payload.client_id;
}

const name = 'Client';
const template = {
  _id: String,
  payload: {
    required: true,
    type: {
      client_id: {
        type: String,
        index: true,
        unique: true,
        required: true,
        immutable: true
      },
      client_name: {
        type: String,
        unique: true,
        required: true,
      },
      client_secret: {
        type: String,
        required: true,
      },
      redirect_uris: {
        type: [String],
        required: true,
        validate: validator.arrOfStrValidator,
      },
      post_logout_redirect_uris: {
        type: [String],
        required: true,
        validate: validator.arrOfStrValidator,
      },
      grant_types: {
        type: [
          {
            type: String,
            required: true,
            // validate: { validator: validateGrantType } // put this on service layer!
          }
        ],
        required: true,
        validate: validator.arrOfStrValidator,
      },
      scope: {
        type: String,
        required: true,
        // validate: { validator: validateScope } // put this on service layer!
      },
      resource_scope: {
        type: String,
        required: false,
        default: ''
      },
      is_internal: {
        type: Boolean,
        required: true,
        default: false
      },
    }
  }
};

const middlewares = [
  { time: 'pre', event: 'validate', fn: setId },
  { time: 'pre', event: 'save', fn: setId }
];

module.exports = {
  enumeration, name, template, middlewares, skip: false
}
