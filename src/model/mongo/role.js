const name = 'Role';
const template = {
  code: {
    type: String,
    index: true,
    unique: true,
    required: true,
  },
  name: {
    type: String,
    unique: true,
    required: true,
  },
  isInternalDefault: {
    type: Boolean,
    required: true,
    default: false
  },
  isExternalDefault: {
    type: Boolean,
    required: true,
    default: false
  }
}

module.exports = { name, template, skip: false }
