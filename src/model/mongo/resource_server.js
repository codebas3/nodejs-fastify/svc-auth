const enumeration = {
  scopes: ['api:read', 'api:add', 'api:update', 'api:remove']
};

const arrOfStrValidator = v => Array.isArray(v) && v.length > 0;

const name = 'ResourceServer';
const template = {
  code: {
    type: String,
    index: true,
    unique: true,
    required: true,
  },
  name: {
    type: String,
    unique: true,
    index: true,
    required: true,
  },
  uri: {
    type: String,
    required: true,
  },
  isPublic: {
    type: Boolean,
    index: true,
    default: true,
    required: true,
  },
  scopes: {
    type: [
      { type: String, required: true }
    ],
    validate: arrOfStrValidator,
  },
}

module.exports = {
  enumeration, name, template, skip: false, validator: { arrOfStrValidator }
}
