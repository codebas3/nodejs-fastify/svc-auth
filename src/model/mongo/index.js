const _ = require('lodash')
const Mongoose = require('mongoose')
const logger = require('../../logger/console')(__filename);
const { stringify, objError } = require('../../logger/lib/format');
const { mongo: opts, constants } = require('../../config');
const Singleton = require('../../util/singleton');
const initModels = require('./init-models');

const {
  TYPE_INFO, TYPE_ERROR, TYPE_WARN, MESSAGE
} = constants;

class Mongo extends Singleton {
  opts;

  model;

  mongoose;

  constructor() {
    super();

    const {
      username: user,
      password: pass,
      database: dbName,
      host, port, family, autoIndex, pool, logConnection,
    } = opts;

    this.opts = {
      user, pass, dbName, host, port, family, autoIndex, pool, logConnection
    }
    this.mongoose = Mongoose;

    if ([undefined, true].indexOf(this.opts.logConnection) > -1) {
      this.mongoose.connection.on('connecting', (...args) => logger.debug(stringify(TYPE_INFO.MONGODB, MESSAGE.MONGODB_POOL_CONNECTING)))
      this.mongoose.connection.on('connected', (...args) => logger.info(stringify(TYPE_INFO.MONGODB, MESSAGE.MONGODB_POOL_CONNECTED)))
      this.mongoose.connection.on('disconnecting', (...args) => logger.warn(stringify(TYPE_WARN.MONGODB, MESSAGE.MONGODB_POOL_DISCONNECTING)))
      this.mongoose.connection.on('disconnected', (...args) => logger.warn(stringify(TYPE_WARN.MONGODB, MESSAGE.MONGODB_POOL_DISCONNECTED)))
      this.mongoose.connection.on('close', (...args) => logger.warn(stringify(TYPE_WARN.MONGODB, MESSAGE.MONGODB_POOL_CLOSED)))
      this.mongoose.connection.on('reconnected', (...args) => logger.debug(stringify(TYPE_INFO.MONGODB, MESSAGE.MONGODB_POOL_RECONNECTED)))
      this.mongoose.connection.on('reconnectFailed', (...args) => logger.error(stringify(TYPE_ERROR.MONGODB, MESSAGE.MONGODB_POOL_RECONNECT_FAIL)))
      this.mongoose.connection.on('error', (...args) => logger.error(stringify(TYPE_ERROR.MONGODB, MESSAGE.MONGODB_POOL_ERROR)))
      this.mongoose.connection.on('fullsetup', (...args) => logger.info(stringify(TYPE_INFO.MONGODB, MESSAGE.MONGODB_POOL_FULLSETUP)))
      this.mongoose.connection.on('all', (...args) => logger.info(stringify(TYPE_INFO.MONGODB, MESSAGE.MONGODB_POOL_ALL_CONNECTED)))
    }
  }

  async connect() {
    const {
      host, port, user, pass, dbName, family, autoIndex, pool
    } = this.opts;

    try {
      await this.mongoose.connect(`mongodb://${host}:${port}`, {
        user, pass, dbName, family, autoIndex, ...pool
      })
      logger.info(stringify(TYPE_INFO.MONGODB, MESSAGE.MONGODB_CONNECTED))

      this.model = initModels(this.mongoose)
    } catch (err) {
      logger.error(stringify(TYPE_ERROR.MONGODB, objError(err)));
      if (err.parent) {
        if (err.parent.message !== err.message) {
          logger.error(stringify(TYPE_ERROR.MONGODB, objError(err.parent)))
        }
      }
      if (err.original) {
        if (err.parent) {
          if (err.original.message !== err.parent.message) {
            logger.error(stringify(TYPE_ERROR.MONGODB, objError(err.original)))
          }
        }
      }

      await this.disconnect();
    }
  }

  async disconnect() {
    return this.mongoose.connection.close();
  }
}

module.exports = Mongo
