const { Schema } = require('mongoose');

const name = 'PageComponent';
const template = {
  code: {
    type: String,
    index: true,
    unique: true,
    required: true,
  },
  name: {
    type: String,
    unique: true,
    required: true,
  },
  isInternalDefault: {
    type: Boolean,
    required: true,
    default: false
  },
  isExternalDefault: {
    type: Boolean,
    required: true,
    default: false
  },
  pageId: {
    type: Schema.Types.ObjectId,
    ref: 'Page',
    required: true,
  },
  roleIds: {
    type: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Role',
        required: true,
      }
    ],
    required: true,
    default: []
  }
}

module.exports = { name, template, skip: false }
