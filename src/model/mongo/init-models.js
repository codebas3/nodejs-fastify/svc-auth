const Mongoose = require('mongoose');
const { unixNow } = require('../../util/timezone');
const collections = require('../../config/mongoCollections');

const { Schema } = Mongoose;

const additionalField = {
  createdBy: { type: String },
  createdAt: {
    type: Number,
    required: true,
    default: unixNow,
  },
  updatedBy: { type: String },
  updatedAt: { type: Number },
  deletedBy: { type: String },
  deletedAt: { type: Number },
  notes: { type: String }
}

const schemas = collections.map(name => ({ file: name, schema: require(`./${name}`) }));
const initModels = (mongoose) => schemas.reduce((all, { file, schema }) => {
  if (!mongoose.models[schema.name] && !schema.skip) {
    const newSchema = new Schema(
      { ...schema.template, ...additionalField },
      { collection: file.toLowerCase() }
    )

    if (schema.middlewares) {
      schema.middlewares.forEach(mw => newSchema[mw.time](mw.event, mw.fn))
    }

    return {
      ...all,
      [schema.name]: mongoose.model(
        schema.name,
        newSchema
      )
    }
  }
  return false;
}, {})

module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
