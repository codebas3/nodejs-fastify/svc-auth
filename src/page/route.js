const {
  sendHomePage, sendLoginPage, login, sendRegistrationPage, registration, sendRegistrationVerifyPage, registrationVerify, logout
} = require('./handler')

module.exports = {
  basePath: '/',
  routes: [
    {
      enabled: true,
      method: 'get',
      path: '/',
      action: [sendHomePage]
    },
    {
      enabled: true,
      method: 'get',
      path: '/login',
      action: [sendLoginPage]
    },
    {
      enabled: true,
      method: 'post',
      path: '/login',
      action: [login]
    },
    {
      enabled: true,
      method: 'get',
      path: '/registration',
      action: [sendRegistrationPage]
    },
    {
      enabled: false,
      method: 'post',
      path: '/registration',
      action: [registration]
    },
    {
      enabled: true,
      method: 'get',
      path: '/registration-verify',
      action: [sendRegistrationVerifyPage]
    },
    {
      enabled: false,
      method: 'post',
      path: '/registration-verify',
      action: [registrationVerify]
    },
    {
      enabled: true,
      method: 'get',
      path: '/logout',
      action: [logout]
    },
  ]
}
