module.exports = {
  route: require('./route'),
  handler: require('./handler'),
  middleware: require('./middleware')
}
