const {
  env, app, auth, hosts
} = require('../config');
const tz = require('../util/timezone');
const encryption = require('../util/simple-encryption');
const Manifest = require('../manifest');
const Chamber = require('../chamber');

const sendHomePage = (req, res) => res.view('index', {
  env,
  project: app.project,
  codename: app.codename,
  host: hosts.svc.auth,
  title: 'Home',
  user: req.uiUser,
  error: req.uiError
})

const sendLoginPage = (req, res) => {
  if (req.uiUser) return res.redirect('/')

  const iat = tz.unixNow()
  return res.view('login', {
    env,
    project: app.project,
    codename: app.codename,
    host: hosts.svc.auth,
    title: 'Sign-in',
    challange: encryption.encode(
      JSON.stringify({ iat, exp: iat + 300 })
    ),
    error: req.uiError
  })
};

const login = (req, res) => {
  if (req.uiUser) return res.redirect('/');

  const { username, password, challange } = req.body;
  if (!(username && challange && challange)) throw new Error('Invalid login payload')

  try {
    const decryptedData = JSON.parse(encryption.decode(challange))
    if (tz.unixNow() > decryptedData.exp) throw new Error('Expired code challange')

    const chamber = Chamber.getInstance();
    const [thisClient] = chamber.client
      .filter(client => client.payload.client_id === 'Du8c84mZqXwMLeVLHx8UWJMt')

    const opts = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: new URLSearchParams({
        client_id: thisClient.payload.client_id,
        client_secret: thisClient.payload.client_secret,
        grant_type: 'password',
        scope: thisClient.payload.scope,
        username,
        password
      }),
    }

    return fetch(`${hosts.svc.auth}/oidc/token`, opts)
      .then((response) => response.json())
      .then((data) => {
        const { hostname } = new URL(hosts.svc.auth);
        const cookieOpt = {
          domain: `.${hostname.split('.').slice(-2).join('.')}`,
          path: '/'
        }

        res
          .setCookie('id_token', data.id_token, cookieOpt)
          .setCookie('access_token', data.access_token, cookieOpt)
          .setCookie('refresh_token', data.refresh_token, cookieOpt)
          .redirect('/')
      });
  } catch (e) {
    req.uiError = e.opensslErrorStack ? new Error('Invalid code challange') : e
    return sendLoginPage(req, res)
  }
};

const sendRegistrationPage = (req, res) => {
  if (req.uiUser) return res.redirect('/');

  return res.view('registration', {
    env,
    project: app.project,
    title: 'Registration',
    user: req.uiUser,
    error: req.uiError
  })
};

const registration = (req, res) => {
  if (req.uiUser) return res.redirect('/');

  return res.send('ok')
};

const sendRegistrationVerifyPage = (req, res) => {
  if (req.uiUser) return res.redirect('/');

  return res.view('registration-verify', {
    env,
    project: app.project,
    title: 'Registration Verification',
    user: req.uiUser,
    error: req.uiError
  })
};

const registrationVerify = (req, res) => {
  if (req.uiUser) return res.redirect('/');

  return res.send('ok')
};

const logout = async (req, res) => {
  if (!req.uiUser) return res.redirect('/login');

  const { redis } = Manifest.getInstance();
  const cookie = req.cookies || {};

  // FIXME: It's should be use access_token!
  if (redis && cookie.id_token) {
    await redis.client.set(`del:id_token:${cookie.id_token}`, 1);
  }

  return res
    .clearCookie('id_token')
    .clearCookie('access_token')
    .clearCookie('refresh_token')
    .redirect('/');
};

module.exports = {
  sendHomePage, sendLoginPage, login, sendRegistrationPage, registration, sendRegistrationVerifyPage, registrationVerify, logout
};
