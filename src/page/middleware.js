const { token: JWT } = require('../util');
const Manifest = require('../manifest');
const { newError } = require('../logger/lib/format');

const cookieAuthentication = async (req, _res) => {
  const cookie = req.cookies || {};
  try {
    if (cookie.access_token && cookie.id_token && cookie.refresh_token) {
      // FIXME: It's should be use access_token!
      req.uiUser = await JWT.decode(cookie.id_token)

      const { redis } = Manifest.getInstance();
      if (redis) {
        // FIXME: It's should be use access_token!
        const isLoggedOut = await redis.client.get(`del:id_token:${cookie.id_token}`);
        if (isLoggedOut) throw newError('invalidSession', req.language)
      }
    } else {
      req.uiUser = undefined;
    }
  } catch (e) {
    req.uiUser = undefined;
    req.uiError = e
  }
}

module.exports = {
  cookieAuthentication
};
