#!env node
/* eslint-disable max-len, no-console */

/**
 * Usage example
 *
 * Generate:
 * $ node bin/token.js generate [data]
 *
 * While data value is JSON.stringify forwat, e.g. :
 * {
 *   sub: 'amir@nodomain.com',
 *   profile: {
 *     id: '1',
 *     uname: 'amirun',
 *     email: 'amir@nodomain.com',
 *     phone: '+6286313149xxx',
 *     name: 'Pak Amir'
 *   },
 *   roles: ['owner'],
 *   scope: 'offline_access',
 *   client_id: 'all-in-one-client',
 *   aud: 'http://localhost:3001/xyz',
 * }
 *
 *
 * Decode:
 * $ node bin/token.js decode [token]
 *
 * While token value is string JWT format, e.g. :
 * eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJh
 * eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJh
 * bmRhaWthbkBub2RvbWFpbi5jb20iLCJwcm9maWxlIjp7ImlkI
 * joiMSIsInVuYW1lIjoiYW5kYWlrYW4iLCJlbWFpbCI6ImFuZG
 * Fpa2FuQG5vZG9tYWluLmNvbSIsInBob25lIjoiKzYyODYzMTM
 * xNDl4eHgiLCJuYW1lIjoiQW5kYWlrYW4ifSwicm9sZXMiOlsi
 * b3duZXIiXSwic2NvcGUiOiJvZmZsaW5lX2FjY2VzcyIsImNsa
 * WVudF9pZCI6ImFsbC1pbi1vbmUtY2xpZW50IiwiYXVkIjoiaH
 * R0cDovL2xvY2FsaG9zdDozMDAxL3h5eiIsImlzcyI6Imh0dHA
 * 6Ly9sb2NhbGhvc3Q6MzAwMCIsImV4cCI6MTcxNjI0NjkwMSwi
 * aWF0IjoxNjg0NzA2OTAxfQ.1vGtk5qSZFjg9Td73BG3VSSZXN
 * ghtaM1HvFqcb0cmkaJNo32mBPpxLZEhBjiYzBxWjkwRRuzZj3
 * wm6AJx6QjJaPDjSpGOIU_ZF1ykio9Op1Lk-A4WwYDgNH0DYUI
 * PKtWtMHU5KVKgTIKnq8axwK-hqqYdjEjvNodaNge6cTH6L_e4
 * mRG0mksOmkIPHkWY18nPV7Q3BKH09c8MWIGNLVQ7J7RDPzYLR
 * CDaDjon4IJUm9soBSCW7slO0ZwXAmPprcqms_YX9IW66BJf91
 * 8MSrdRQ9E1_0U5y_ytV0tig6pj4UhaZwCgl7Ph9FVDKsGxDIF
 * G02N-Yk8nHfJxISDTXD-yA
 */

require('dotenv').config()

const { env, hosts } = require('../config');
const { generate, decode } = require('./token');

const _duration = 3.154e+7 // 1 year

function getParams() {
  const flag = process.argv[2] || 'generate';
  const payload = process.argv[3];

  return { flag, payload, duration: _duration }
}

function main({ flag, duration, payload }) {
  const timerange = duration || _duration;
  const now = Math.floor(new Date().getTime() / 1000);

  if (flag !== 'generate') {
    if (env !== 'test') console.info('\nDecoding token:', payload);
    return decode(payload).then((data) => {
      if (require.main === module) return console.log('Decoded token:', data)
      return data
    })
  }

  const data = require.main === module ? JSON.parse(payload) : payload;
  data.iss = hosts.svc.auth
  data.exp = now + timerange

  if (env !== 'test') console.info('\nGenerating token:', data);
  return generate(data).then((token) => {
    if (require.main === module) return console.log('Token value:', token)
    return token
  });
}

if (require.main === module) main(getParams());
else module.exports = main;
