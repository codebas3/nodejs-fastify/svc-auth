// How to use?
// const { esm: { storage, importer } } = require('./util');
// await importer([ 'got', 'oidc-provider' ])

const Promise = require('bluebird');

const storage = {};

const _importer = async (moduleName) => {
  if (!moduleName) return Promise.reject(new Error('Param moduleName required'));

  if (storage[moduleName]) return Promise.resolve(storage[moduleName]);
  return import(moduleName).then((lib) => {
    storage[moduleName] = lib
    return Promise.resolve(storage[moduleName])
  }).catch(Promise.reject)
}

const importer = (moduleName) => {
  if (moduleName instanceof Array) return Promise.map(moduleName, _importer)
  return _importer(moduleName)
}

module.exports = { storage, importer };
