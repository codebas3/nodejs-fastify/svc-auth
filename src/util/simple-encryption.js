const { publicEncrypt, privateDecrypt } = require('crypto');

const { auth } = require('../config');

const encode = (str) => {
  const challange = publicEncrypt(auth.public, Buffer.from(str));

  return challange.toString('hex');
};

const decode = (str) => {
  const decrypted = privateDecrypt(
    auth.secretPkcs8,
    Buffer.from(str, 'hex')
  );

  return decrypted.toString('utf-8')
};

module.exports = { encode, decode };
