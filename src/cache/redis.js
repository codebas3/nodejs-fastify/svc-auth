const fs = require('fs');
const { createClient } = require('redis');
const logger = require('../logger/console')(__filename);
const { stringify, objError } = require('../logger/lib/format');
const { redis: opts, constants } = require('../config');
const Singleton = require('../util/singleton');
const { TYPE_WARN } = require('../config/constants');

const { TYPE_INFO, TYPE_ERROR, MESSAGE } = constants;

class Redis extends Singleton {
  opts;

  client;

  connected = false;

  constructor() {
    super();

    const me = this;

    me.opts = opts;

    const redisPassword = fs.existsSync(opts.password)
      ? fs.readFileSync(opts.password).toString().replace(/\n$/, '')
      : opts.password;

    const acl = redisPassword ? `${opts.username}:${redisPassword}@` : '';
    const config = {
      url: `redis://${acl}${opts.host}:${opts.port}`
    };

    this.client = createClient(config);

    this.client.on('connect', async () => {
      me.connected = true;
      logger.info(stringify(
        TYPE_INFO.REDIS,
        MESSAGE.REDIS_CONNECTED
      ));
    });

    this.client.on('end', async () => {
      me.connected = false;
      logger.warn(stringify(
        TYPE_WARN.REDIS,
        MESSAGE.REDIS_NOT_CONNECTED
      ));
    });

    this.client.on('ready', async () => {
      me.connected = true;
      logger.info(stringify(
        TYPE_INFO.REDIS,
        MESSAGE.REDIS_READY_TO_USE
      ));
    });

    this.client.on('error', (error) => {
      me.connected = false;
      logger.error(stringify(
        TYPE_ERROR.REDIS,
        objError(error)
      ));
    });
  }

  async connect() {
    await this.client.connect();
    await this.client.select(this.opts.db);
  }

  async disconnect() {
    return this.client.disconnect();
  }
}

module.exports = Redis;
