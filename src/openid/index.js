module.exports = {
  OIDC: require('./oidc'),
  route: require('./route'),
  handler: require('./handler'),
  middleware: require('./middleware')
}
