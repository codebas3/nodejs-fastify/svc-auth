const {
  interaction, login, abort, confirmation, repost, federated
} = require('./handler')

module.exports = {
  basePath: '/oidc',
  routes: [
    {
      enabled: true,
      method: 'get',
      path: '/_tldr/:uid',
      action: [interaction]
    },
    {
      enabled: true,
      method: 'post',
      path: '/_tldr/:uid/login',
      action: [login]
    },
    {
      enabled: true,
      method: 'post',
      path: '/_tldr/:uid/confirm',
      action: [confirmation]
    },
    {
      enabled: true,
      method: 'post',
      path: '/_tldr/:uid/federated',
      action: [federated]
    },
    {
      enabled: true,
      method: 'get',
      path: '/_tldr/:uid/abort',
      action: [abort]
    },
    {
      enabled: false,
      method: 'get',
      path: '/_tldr/callback/google',
      action: [repost]
    }
  ]
}
