/* eslint-disable no-new, no-unused-vars, object-property-newline, prefer-regex-literals, no-use-before-define */

const { objError, stringify } = require('../../logger/lib/format');
const { TYPE_ERROR } = require('../../config/constants');
const logger = require('../../logger/console')(__filename);
const RegistrantService = require('../grpc-client/registrant');
const AccountService = require('../grpc-client/account');

const store = new Map();
const logins = new Map();

const EmailRegExp = new RegExp(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/g)
const JSONParse = (value) => {
  try {
    return JSON.parse(value)
  } catch (e) {
    return value
  }
}

async function Login(username, password) {
  const registrantService = new RegistrantService();

  let registrant = {};
  try {
    registrant = await registrantService.login({ email: username, password })
  } catch (err) {
    logger.error(stringify(TYPE_ERROR.AUTHENTICATION, objError(err)))
  }
  const user = {
    accountId: null,
    usrRegistrantId: registrant.id,
    uname: null,
    email: registrant.email,
    emailVerification: JSONParse(registrant.emailVerification),
    phone: registrant.phone,
    phoneVerification: JSONParse(registrant.phoneVerification),
    name: registrant.name,
    gender: null,
    placeOfBirth: null,
    birthOfDate: null,
    avatar: null,
    notes: registrant.notes,
    roles: []
  }

  const accountService = new AccountService();
  const email = (username.match(EmailRegExp) || [])[0]
  const uname = email ? '' : email;

  let account = {}
  try {
    account = await accountService.login({ email, uname, password })
  } catch (err) {
    logger.error(stringify(TYPE_ERROR.AUTHENTICATION, objError(err)))
  }
  const {
    id: accountId, usrRegistrantId,
    uname: _uname,
    email: _email,
    emailVerification, phone, phoneVerification,
    name, gender, placeOfBirth, birthOfDate,
    avatar, notes, roles
  } = account;

  Object.assign(user, {
    accountId,
    usrRegistrantId,
    uname: _uname,
    email: _email,
    emailVerification: JSONParse(emailVerification),
    phone,
    phoneVerification: JSONParse(phoneVerification),
    name, gender, placeOfBirth, birthOfDate,
    avatar, notes
  })

  if (roles) user.roles = account.roles.map(role => role.code)
  if (user.accountId || user.usrRegistrantId) {
    new Account(user.email, user);
    return store.get(user.email);
  }
  return new Error('Username or password missmatch')
}

async function LoginVerifyCode(username, code) {
  const registrantService = new RegistrantService();

  let registrant = {};
  try {
    registrant = await registrantService.loginRegistrantSecretCode({ email: username, code })
  } catch (err) {
    throw new Error(err)
  }
  const user = {
    accountId: null,
    usrRegistrantId: registrant.id,
    uname: null,
    email: registrant.email,
    emailVerification: JSONParse(registrant.emailVerification),
    phone: registrant.phone,
    phoneVerification: JSONParse(registrant.phoneVerification),
    name: registrant.name,
    gender: null,
    placeOfBirth: null,
    birthOfDate: null,
    avatar: null,
    notes: registrant.notes,
    roles: []
  }

  const email = (username.match(EmailRegExp) || [])[0]

  let account = {}
  try {
    account = await registrantService.verify({ email, code })
  } catch (err) {
    throw new Error(err)
  }
  const {
    id: accountId, usrRegistrantId,
    uname: _uname,
    email: _email,
    emailVerification, phone, phoneVerification,
    name, gender, placeOfBirth, birthOfDate,
    avatar, notes, roles
  } = account;

  Object.assign(user, {
    accountId,
    usrRegistrantId,
    uname: _uname,
    email: _email,
    emailVerification: JSONParse(emailVerification),
    phone,
    phoneVerification: JSONParse(phoneVerification),
    name, gender, placeOfBirth, birthOfDate,
    avatar, notes
  })

  user.roles = [JSON.parse(notes).role]

  if (user.accountId || user.usrRegistrantId) {
    new Account(user.email, user);
    return store.get(user.email);
  }
  return new Error('Username or password missmatch')
}

class Account {
  constructor(accountId, profile) {
    // console.debug('\n%% constructor', arguments)
    this.accountId = accountId;
    this.profile = profile;
    this.roles = profile.roles;
    delete this.profile.roles;

    store.set(this.accountId, this);
  }

  /**
   * @param use - can either be "id_token" or "userinfo", depending on
   *   where the specific claims are intended to be put in.
   * @param scope - the intended scope, while oidc-provider will mask
   *   claims depending on the scope automatically you might want to skip
   *   loading some claims from external resources etc. based on this detail
   *   or not return them in id tokens but only userinfo and so on.
   */
  async claims(use, scope) {
    // console.debug('\n%% claims', arguments, this.profile)
    return {
      sub: this.accountId, // it is essential to always return a sub claim
      profile: this.profile,
      roles: this.roles,
    };
  }

  // FIXME: this should be fixed in the future!
  static async findByFederated(provider, claims) {
    // console.debug('\n%% findByFederated', arguments)
    const id = `${provider}.${claims.sub}`;
    if (!logins.get(id)) {
      logins.set(id, new Account(id, claims));
    }
    return logins.get(id);
  }

  static async findAccountByLogin({ username, password }) {
    return Login(username, password)
  }

  static async findAccount(ctx, id, _token) {
    // console.debug('\n%% findAccount', arguments)
    // token is a reference to the token used for which a given account is being loaded,
    //   it is undefined in scenarios where account claims are returned from authorization endpoint
    // ctx is the koa request context
    const profile = ctx.oidc.body || {};
    const { username, password } = profile
    if (username && password) return Login(username, password)
    if (id) return store.get(id)
    return new Error('Someting error with find account')
  }

  static async findAccountByVerifyCode(ctx, id) {
    // console.debug('\n%% findAccount', arguments)
    // token is a reference to the token used for which a given account is being loaded,
    //   it is undefined in scenarios where account claims are returned from authorization endpoint
    // ctx is the koa request context
    const profile = ctx.oidc.body || {};
    const { username, code } = profile
    if (username && code) return LoginVerifyCode(username, code)
    if (id) return store.get(id)
    return new Error('Someting error with find account')
  }
}

module.exports = Account;
