/* eslint-disable camelcase */

const { hosts } = require('../config')
const { renderError } = require('./model');

let SessionNotFound;
import('oidc-provider').then(lib => {
  SessionNotFound = lib.errors.SessionNotFound
})

const noCache = async (_req, res, next) => {
  res.setHeader('cache-control', 'no-cache, no-store');

  try {
    await next()
  } catch (err) {
    const { message: error, error_description } = err

    SessionNotFound = SessionNotFound || (await import('oidc-provider')).errors.SessionNotFound
    if (err instanceof SessionNotFound) {
      console.debug('\n%% noCache', { status: err.status, error, error_description })
      res.statusCode = err.status
    }

    renderError(res, { error, error_description }, err);
  }
}

module.exports = { noCache }
