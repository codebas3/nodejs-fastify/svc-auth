const { env, app } = require('../config');
const Adapter = require('./adapter');
const AccountService = require('./service/account.fake');

module.exports = {
  findAccount: AccountService.findAccount,
  adapter: Adapter,
  clientBasedCORS(ctx, origin, client) {
    // ctx.oidc.route can be used to exclude endpoints from this behaviour, in that case just return
    // true to always allow CORS on them, false to deny
    // you may also allow some known internal origins if you want to
    return true
  },
  claims: {
    address: ['address'],
    email: ['email', 'email_verified'],
    phone: ['phone_number', 'phone_number_verified'],
    profile: [
      'birthdate', 'family_name', 'gender', 'given_name', 'locale',
      'middle_name', 'name', 'nickname', 'picture', 'preferred_username',
      'profile', 'updated_at', 'website', 'zoneinfo'
    ]
  },
  /**
   * Function used to add additional claims to an Access Token when it is being issued.
   * For opaque Access Tokens these claims will be stored in your storage under the extra property
   * and returned by introspection as top level claims.
   * For jwt Access Tokens these will be top level claims.
   * Returned claims will not overwrite pre-existing top level claims.
   * @param {*} ctx
   * @param {*} token
   * @returns
   */
  extraTokenClaims(ctx, token) {
    // const profile = ctx.oidc.entities.Account.profile || {};
    // const roles = ctx.oidc.entities.Account.role || [];
    // return { profile, roles };

    return {
      readme: 'find me on oidc.config to change this!',
    };
  },
  interactions: {
    url(_ctx, interaction) {
      return `/oidc/_tldr/${interaction.uid}`
    },
  },
  cookies: {
    keys: app.cookies,
    long: {
      // httpOnly: true,
      // overwrite: true,
      // sameSite: 'none',
      signed: true
    },
    short: {
      // httpOnly: true,
      // overwrite: true,
      // sameSite: 'lax',
      signed: true
    },
  },
  features: {
    clientCredentials: { enabled: true },
    introspection: {
      enabled: true,
      allowedPolicy(ctx, client, token) {
        if (
          client.introspectionEndpointAuthMethod === 'none'
          && token.clientId !== ctx.oidc.client?.clientId
        ) {
          return false;
        }
        return true;
      },
    },
    devInteractions: { enabled: false }, // defaults to true
    deviceFlow: { enabled: true }, // defaults to false
    revocation: { enabled: true }, // defaults to false
    resourceIndicators: {
      // ATTENTION! this is handling of body.payload.resource with value "String"
      enabled: true,
      defaultResource(ctx, client, oneOf) {
        // INFO: executed when req.body.resource is empty
        console.debug('\n%% resourceIndicators.defaultResource', {
          'ctx.oidc.params': ctx.oidc.params,
          client,
          oneOf
        })

        return Array.isArray(ctx.oidc.params?.resource)
          ? ctx.oidc.params?.resource[0]
          : ctx.oidc.params?.resource;
      },
      getResourceServerInfo(ctx, resourceIndicator, client) {
        console.debug('\n%% resourceIndicators.getResourceServerInfo', {
          resourceIndicator,
          client,
          'ctx.oidc': ctx.oidc,
          url: ctx.request.url,
          params: ctx.oidc.params,
          body: ctx.oidc.body
        })

        const options = {};
        if (resourceIndicator) options.audience = resourceIndicator
        if (ctx.oidc.params.scope) {
          options.scope = ctx.oidc.params.scope.split(' ')
            .filter(s => s.indexOf(':') > -1)
            .concat('offline_access').join(' ');
        }

        options.accessTokenTTL = 60 * 60; // 1 hours

        if (resourceIndicator) {
          options.accessTokenFormat = 'jwt';
          options.jwt = {
            sign: { alg: 'RS256' },
          }
        }

        return options
      },
      async useGrantedResource(_ctx, _model) {
        return true;
      }
    },
  },
  ttl: {
    AccessToken: function AccessTokenTTL(ctx, token, client) {
      if (token.resourceServer) return token.resourceServer.accessTokenTTL;
      return 60 * 60; // 1 hour in seconds
    },
    AuthorizationCode: 600 /* 10 minutes in seconds */,
    BackchannelAuthenticationRequest:
      function BackchannelAuthenticationRequestTTL(ctx, request, client) {
        if (ctx && ctx.oidc && ctx.oidc.params?.requested_expiry) {
          return Math.min(10 * 60, ctx.oidc.params?.requested_expiry); // 10 minutes in seconds or requested_expiry, whichever is shorter
        }
        return 10 * 60; // 10 minutes in seconds
      },
    ClientCredentials: function ClientCredentialsTTL(ctx, token, client) {
      if (token.resourceServer) {
        return token.resourceServer.accessTokenTTL || 10 * 60; // 10 minutes in seconds
      }
      return 10 * 60; // 10 minutes in seconds
    },
    DeviceCode: 600 /* 10 minutes in seconds */,
    Grant: 1209600 /* 14 days in seconds */,
    IdToken: 3600 /* 1 hour in seconds */,
    Interaction: 3600 /* 1 hour in seconds */,
    RefreshToken: function RefreshTokenTTL(ctx, token, client) {
      if (
        ctx
        && ctx.oidc.entities.RotatedRefreshToken
        && client.applicationType === 'web'
        && client.tokenEndpointAuthMethod === 'none'
        && !token.isSenderConstrained()
      ) {
        // Non-Sender Constrained SPA RefreshTokens do not have infinite expiration through rotation
        return ctx.oidc.entities.RotatedRefreshToken.remainingTTL;
      }
      return 14 * 24 * 60 * 60; // 14 days in seconds
    },
    Session: 1209600 /* 14 days in seconds */,
  },
  async renderError(ctx, out, error) {
    // FIXME: Should be handled by fastify.setErrorHandler

    console.debug('\n%% renderError', { out, error })

    const body = {
      error: {
        name: error.error_detail ? error.message : null,
        message: error.error_detail ? error.error_detail : error.message,
        description: error.error_description || null,
        stack: env === 'production' ? undefined : error.stack.toString().split('\n')
      }
    }

    try {
      // ctx is koa context (req and response)
      ctx.type = 'json';
      ctx.body = body;
    } catch (e) {
      // ctx is http.ServerResponse
      ctx.writeHead(200, { 'Content-Type': 'application/json' });
      ctx.end(JSON.stringify(body));
    }
  }
}
