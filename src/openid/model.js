const Mongo = require('../model/mongo');

const Grantable = new Set([
  'access_token',
  'authorization_code',
  'refresh_token',
  'device_code',
  'backchannel_authentication_request',

  'session',
  'client_credentials',
  'client',
  'initial_access_token',
  'registration_access_token',
  'interaction',
  'replay_detection',
  'pushed_authorization_request',
  'grant'
]);

class Model extends Set {
  add(name) {
    const nu = this.has(name);
    super.add(name);

    if (!nu) {
      const { mongoose } = Mongo.getInstance();

      mongoose.connection.db.collection(name)
        .createIndexes([
          ...(Grantable.has(name) ? [{ key: { 'payload.grantId': 1 } }] : []),
          ...(name === 'device_code' ? [{ key: { 'payload.userCode': 1 }, unique: true }] : []),
          ...(name === 'session' ? [{ key: { 'payload.uid': 1 }, unique: true }] : []),
          { key: { expiresAt: 1 }, expireAfterSeconds: 0 },
        ])
        .catch(console.error);
    }
  }
}

module.exports = new Model()
