/* eslint-disable max-classes-per-file */

const { snakeCase } = require('lodash');

const Mongo = require('../model/mongo');

class MongoAdapter {
  // this model is specific for builtin OIDC schema, it's different with our ./src/model
  model = require('./model');

  constructor(name) {
    this.name = snakeCase(name);

    // NOTE: you should never be creating indexes at runtime in production, the following is in
    //   place just for demonstration purposes of the indexes required
    this.model.add(this.name);
  }

  // NOTE: the payload for Session model may contain client_id as keys, make sure you do not use
  //   dots (".") in your client_id value charset.
  async upsert(_id, payload, expiresIn) {
    let expiresAt;

    if (expiresIn) {
      expiresAt = new Date(Date.now() + (expiresIn * 1000));
    }

    const result = await this.coll().updateOne(
      { _id },
      { $set: { payload, ...(expiresAt ? { expiresAt } : undefined) } },
      { upsert: true },
    );

    return result
  }

  async find(_id) {
    const result = await this.coll().find(
      { _id },
      { payload: 1 },
    ).limit(1).next();

    if (!result) return undefined;
    return result.payload;
  }

  async findByUserCode(userCode) {
    const result = await this.coll().find(
      { 'payload.userCode': userCode },
      { payload: 1 },
    ).limit(1).next();

    if (!result) return undefined;
    return result.payload;
  }

  async findByUid(uid) {
    const result = await this.coll().find(
      { 'payload.uid': uid },
      { payload: 1 },
    ).limit(1).next();

    if (!result) return undefined;
    return result.payload;
  }

  async destroy(_id) {
    await this.coll().deleteOne({ _id });
  }

  async revokeByGrantId(grantId) {
    await this.coll().deleteMany({ 'payload.grantId': grantId });
  }

  async consume(_id) {
    await this.coll().findOneAndUpdate(
      { _id },
      { $set: { 'payload.consumed': Math.floor(Date.now() / 1000) } },
    );
  }

  coll(name) {
    return this.constructor.coll(name || this.name);
  }

  static coll(name) {
    const { mongoose } = Mongo.getInstance()

    return mongoose.connection.db.collection(name);
  }
}

module.exports = MongoAdapter;
