const { strict: assert } = require('assert')
const crypto = require('crypto')
const { inspect } = require('util')
const { isEmpty } = require('lodash')

const logger = require('../logger/console')(__filename);
const { stringify, objError } = require('../logger/lib/format');
const {
  env, app, hosts, constants: { TYPE_ERROR }
} = require('../config');
const Account = require('./service/account.fake')

const keys = new Set()

const debug = (obj, text) => {
  if (!text) return JSON.stringify(obj, 0, 2);
  return (new URLSearchParams(
    Object.entries(obj).reduce((acc, [key, value]) => {
      keys.add(key)
      if (isEmpty(value)) return acc
      acc[key] = inspect(value, { depth: null })
      return acc
    }, {}),
    '<br/>',
    ': ',
    {
      encodeURIComponent(value) {
        return keys.has(value) ? `<strong>${value}</strong>` : value
      },
    }
  ).toString())
}

const interaction = async (req, res, next) => {
  const OIDC = require('./oidc');
  const { provider } = OIDC.getInstance();

  try {
    const {
      uid, prompt, params, session
    } = await provider.interactionDetails(req.raw, res.raw)
    const client = await provider.Client.find(params.client_id)

    switch (prompt.name) {
    case 'login': {
      return res.view('oidc/client-login', {
        env,
        host: hosts.svc.auth,
        project: app.project,
        client,
        uid,
        details: prompt.details,
        params: JSON.stringify(params),
        title: 'Sign-in',
        // google: req.google,
        session: session ? debug(session) : undefined,
        dbg: {
          params: debug(params),
          prompt: debug(prompt),
        },
      })
    }
    case 'consent': {
      return res.view('oidc/client-consent', {
        env,
        host: hosts.svc.auth,
        project: app.project,
        client,
        uid,
        details: prompt.details,
        params,
        title: 'Authorization',
        session: session ? debug(session) : undefined,
        dbg: {
          params: debug(params),
          prompt: debug(prompt),
        },
      })
    }
    default:
      return next()
    }
  } catch (err) {
    logger.error(stringify(TYPE_ERROR.UNCAUGHT, objError(err)));
    throw err
  }
}

const login = async (req, res) => {
  const OIDC = require('./oidc');
  const { provider } = OIDC.getInstance();

  const { returnTo, prompt: { name } } = await provider.interactionDetails(req.raw, res.raw);
  assert.equal(name, 'login')

  const account = await Account.findAccountByLogin(req.body)
  if (account instanceof Error) {
    return res.redirect(
      `${returnTo.replace('auth', 'oidc/_tldr')}?error=${account.message}`
    )
  }

  const result = {
    login: { accountId: account.accountId },
  }
  return provider.interactionFinished(req.raw, res.raw, result, {
    mergeWithLastSubmission: false,
  })
}

const repost = (req, res) => {
  const nonce = res.locals.cspNonce
  return res.view('oidc/federated-repost', {
    host: hosts.svc.auth,
    project: app.project,
    layout: false,
    upstream: 'google',
    nonce
  })
}

const federated = async (req, res) => {
  const OIDC = require('./oidc');
  const { provider } = OIDC.getInstance();

  const {
    prompt: { name },
  } = await provider.interactionDetails(req.raw, res.raw)
  assert.equal(name, 'login')

  const path = `/oidc/_tldr/${req.params.uid}/federated`

  switch (req.body.upstream) {
  case 'google': {
    const callbackParams = req.google.callbackParams(req.raw)

    // init
    if (!Object.keys(callbackParams).length) {
      const state = `${req.params.uid}|${crypto.randomBytes(32).toString('hex')}`
      const nonce = crypto.randomBytes(32).toString('hex')

      req.cookies.set('google.state', state, { path, sameSite: 'strict' })
      req.cookies.set('google.nonce', nonce, { path, sameSite: 'strict' })

      res.status(303)
      return res.redirect(
        req.google.authorizationUrl({
          state,
          nonce,
          scope: 'openid email profile',
        })
      )
    }

    // callback
    const state = req.cookies.get('google.state')
    req.cookies.set('google.state', null, { path })
    const nonce = req.cookies.get('google.nonce')
    req.cookies.set('google.nonce', null, { path })

    const tokenset = await req.google.callback(undefined, callbackParams, { state, nonce, response_type: 'id_token' })
    const account = await Account.findByFederated('google', tokenset.claims())

    const result = {
      login: {
        accountId: account.accountId,
      },
    }
    return provider.interactionFinished(req.raw, res.raw, result, {
      mergeWithLastSubmission: false,
    })
  }
  default:
    return undefined
  }
}

const confirmation = async (req, res) => {
  const OIDC = require('./oidc');
  const { provider } = OIDC.getInstance();

  const interactionDetails = await provider.interactionDetails(req.raw, res.raw)
  const {
    prompt: { name, details },
    params,
    session: { accountId },
  } = interactionDetails
  assert.equal(name, 'consent')

  let { grantId } = interactionDetails
  let grant

  if (grantId) {
    // we'll be modifying existing grant in existing session
    grant = await provider.Grant.find(grantId)
  } else {
    // we're establishing a new grant
    grant = new provider.Grant({
      accountId,
      clientId: params.client_id,
    })
  }

  if (details.missingOIDCScope) {
    grant.addOIDCScope(details.missingOIDCScope.join(' '))
  }
  if (details.missingOIDCClaims) {
    grant.addOIDCClaims(details.missingOIDCClaims)
  }
  if (details.missingResourceScopes) {
    // eslint-disable-next-line no-restricted-syntax
    for (const [indicator, scope] of Object.entries(details.missingResourceScopes)) {
      grant.addResourceScope(indicator, scope.join(' '))
    }
  }

  grantId = await grant.save()

  const consent = {}
  if (!interactionDetails.grantId) {
    // we don't have to pass grantId to consent, we're just modifying existing one
    consent.grantId = grantId
  }

  const result = { consent }
  return provider.interactionFinished(req.raw, res.raw, result, {
    mergeWithLastSubmission: true,
  })
}

const abort = async (req, res) => {
  const OIDC = require('./oidc');
  const { provider } = OIDC.getInstance();

  const result = {
    error: 'access_denied',
    error_description: 'End-User aborted interaction',
  }

  return provider.interactionFinished(req.raw, res.raw, result, {
    mergeWithLastSubmission: false,
  })
}

module.exports = {
  interaction,
  repost,
  login,
  federated,
  confirmation,
  abort
};
