const { auth, hosts } = require('../config');
const { JwkImporter, Singleton } = require('../util');
const Mongo = require('../model/mongo');
const Grant = require('./grant');
const Option = require('./option');

class OIDC extends Singleton {
  opts;

  jwkImporter = new JwkImporter();

  constructor() {
    super();
    this.opts = Option;
  }

  static async saveJWT(ctx, next) {
    const { mongoose } = Mongo.getInstance();

    await next();

    // Just save JWT if any, but let it go if is none
    try {
      if (!ctx.body.access_token) throw new Error('Finding access_token value, but context.body is undefined')
      if (ctx.body.access_token.length >= 100) {
        await mongoose.connection.db.collection('access_token_jwt').insertOne({
          _id: ctx.body.access_token,
          payload: ctx.body
        })
      }
    } catch (error) {
      // logger.warn(stringify(TYPE_WARN.AUTHENTICATION, objError(error)));
    }
  }

  async start() {
    const me = this;

    // Generate JWK
    const privateJwk = await me.jwkImporter.private(auth.secretPkcs8, auth.algorithm)
    me.opts.jwks = {
      // You can use multiple asymmetric keys
      // see https://github.com/panva/node-oidc-provider/tree/main/docs#jwks
      keys: [privateJwk]
    }

    const { default: Provider } = await import('oidc-provider')
    me.provider = new Provider(hosts.svc.auth, me.opts);

    const gtyPassword = await Grant.password();
    me.provider.registerGrantType(
      gtyPassword.gty,
      gtyPassword.handler,
      gtyPassword.parameters
    )

    // Oidc will runing over proxified
    this.provider.proxy = true;
    this.provider.app.middleware.unshift(OIDC.saveJWT)

    return this;
  }
}

module.exports = OIDC;
