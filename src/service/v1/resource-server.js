/* eslint-disable class-methods-use-this */

const Promise = require('bluebird');

const logger = require('../../logger/console')(__filename);
const { stringify, objError } = require('../../logger/lib/format');
const { rabbitmq, constants } = require('../../config');
const Mongo = require('../../model/mongo');
const { Publisher, Client: RMQ } = require('../../queue/rabbitmq');

const { TYPE_INFO, TYPE_ERROR } = constants

class ResourceServer {
  static caller() {
    // * This is fake error for getting where the method call from;
    // * in case, we can't use arguments.caller in strict mode
    const e = new Error();
    return e.stack.split('\n')[3].trim().split(/\s/g)[1]
  }

  static logInfo(options, result) {
    logger.info(stringify(
      `${TYPE_INFO.SERVICE_V1}: ${ResourceServer.caller()}`,
      ({
        returned: result.rows ? result.rows.length : result.length,
        options
      })
    ));
  }

  static logError(err) {
    logger.error(stringify(
      `${TYPE_ERROR.SERVICE_V1}: ${ResourceServer.caller()}`,
      objError(err)
    ));
  }

  static async publish(message) {
    const rmq = RMQ.getInstance()
    const { ResourceServer: q } = rabbitmq.queue;
    const publisher = new Publisher(rmq, q.exchange, rmq.channel)
    publisher.setOpts({ persistence: false, mandatory: true })
    publisher.publish(q.route, message, null, (err) => {
      if (err) ResourceServer.logError(err)
    })
  }

  async create(data, options, writer) {
    const { repo } = Mongo.getInstance()
    const result = await repo.resourceServer.create(data, options, writer);
    ResourceServer.logInfo({ data, options, writer }, result)

    // send to queue
    if (result.count) ResourceServer.publish({ msg: 'create' })

    return result;
  }

  async getBy(options) {
    const { repo } = Mongo.getInstance()
    const result = await repo.resourceServer.getBy(options);
    ResourceServer.logInfo({ options }, result)
    return result;
  }

  async getOneBy(options) {
    const { repo } = Mongo.getInstance()
    const result = await repo.resourceServer.getOneBy(options);
    ResourceServer.logInfo({ options }, result)
    return result;
  }

  async deleteBy(options, writer) {
    const { repo } = Mongo.getInstance()
    const { rows: [before] } = await repo.resourceServer.getOneBy(options);
    const result = await repo.resourceServer.deleteBy(options, writer);
    ResourceServer.logInfo({ options, writer }, result)

    if (result.count) {
      const { rows } = await repo.client.getBy({
        count: false,
        limit: Infinity,
        where: {
          'payload.scope': { $regex: `.*${before.code}_.*` }
        }
      })

      // let's make this async
      Promise.each(rows, ({ _id, payload }) => {
        const scope = payload.scope.split(' ').map(s => {
          if (s.indexOf(before.code) !== 0) return s
          return ''
        }).filter(s => s).join(' ')

        repo.client.updateBy({ payload: { ...payload, scope } }, {
          where: { _id }
        })
      })

      await require('../../queue-consumer/rabbitmq/endpoint').sync()

      // send to queue
      ResourceServer.publish({ msg: 'delete' })
    }

    return result;
  }

  async updateBy(data, options, writer) {
    const { repo } = Mongo.getInstance()

    const { rows: [before] } = await repo.resourceServer.getOneBy(options);
    const result = await repo.resourceServer.updateBy(data, options, writer);
    ResourceServer.logInfo({ data, options, writer }, result)

    if (result.count) {
      const { rows: [after] } = await repo.resourceServer.getOneBy(options);
      if (before.code !== after.code) {
        const { rows } = await repo.client.getBy({
          count: false,
          limit: Infinity,
          where: {
            'payload.scope': { $regex: `.*${before.code}_.*` }
          }
        })

        // let's make this async
        Promise.each(rows, ({ _id, payload }) => {
          const scope = payload.scope.split(' ').map(s => {
            if (s.indexOf(before.code) !== 0) return s
            return (after.code + scope.substring(before.code.length))
          }).filter(s => s).join(' ')

          repo.client.updateBy({ payload: { ...payload, scope } }, {
            where: { _id }
          })
        })
      }

      await require('../../queue-consumer/rabbitmq/endpoint').sync()

      // send to queue
      ResourceServer.publish({ msg: 'update' })
    }

    return result;
  }
}

module.exports = ResourceServer
