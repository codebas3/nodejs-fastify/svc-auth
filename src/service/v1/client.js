/* eslint-disable class-methods-use-this */

const logger = require('../../logger/console')(__filename);
const { stringify, objError } = require('../../logger/lib/format');
const { rabbitmq, constants } = require('../../config');
const Mongo = require('../../model/mongo');
const { enumeration } = require('../../model/mongo/client');
const Chamber = require('../../chamber');
const { Publisher, Client: RMQ } = require('../../queue/rabbitmq');

const { TYPE_INFO, TYPE_ERROR } = constants

class Client {
  static caller() {
    // * This is fake error for getting where the method call from;
    // * in case, we can't use arguments.caller in strict mode
    const e = new Error();
    return e.stack.split('\n')[3].trim().split(/\s/g)[1]
  }

  static logInfo(options, result) {
    logger.info(stringify(
      `${TYPE_INFO.SERVICE_V1}: ${Client.caller()}`,
      ({
        returned: result.rows ? result.rows.length : result.length,
        options
      })
    ));
  }

  static logError(err) {
    logger.error(stringify(
      `${TYPE_ERROR.SERVICE_V1}: ${Client.caller()}`,
      objError(err)
    ));
  }

  static async publish(message) {
    const rmq = RMQ.getInstance()
    const { Client: q } = rabbitmq.queue;
    const publisher = new Publisher(rmq, q.exchange, rmq.channel)
    publisher.setOpts({ persistence: false, mandatory: true })
    publisher.publish(q.route, message, null, (err) => {
      if (err) Client.logError(err)
    })
  }

  #validateGrantType(payload = {}) {
    if (!payload.grant_types) return [];

    const values = payload.grant_types = (payload.grant_types || []).filter(el => el.trim());
    if (!values.length) return [];

    const invalids = []
    const { grantTypes: Enum } = enumeration;
    values.forEach(el => {
      if (Enum.indexOf(el) === -1) invalids.push(el);
    })

    return invalids
  }

  async #validateScope(payload = {}) {
    if (!payload.scope) return [];

    const value = payload.scope = (payload.scope || '').split(' ').filter(el => el.trim()).join(' ');
    if (!value) return [];

    const invalids = [];
    const { scopes: Enum } = enumeration;
    value.split(' ').forEach(el => {
      if (Enum.indexOf(el) === -1) invalids.push(el)
    });

    return invalids;
  }

  async #validateResourceScope(payload = {}) {
    if (!payload.resource_scope) return [];

    const value = payload.resource_scope = (payload.resource_scope || '').split(' ').filter(el => el.trim()).join(' ');
    if (!value) return [];

    const scopes = []
    const chamber = Chamber.getInstance()
    chamber.resource_server.forEach(rs => {
      rs.scopes.forEach(scope => scopes.push(`${rs.code}:${scope}`))
    })

    const invalids = [];
    value.split(' ').forEach(s => {
      if (scopes.indexOf(s) === -1) invalids.push(s)
    })

    return invalids
  }

  async validation(data) {
    const errors = []
    const invalidGrantType = await this.#validateGrantType(data.payload);
    if (invalidGrantType.length) errors.push(`Invalid grant_types for: ${invalidGrantType.join(', ')}`)

    const invalidScopes = await this.#validateScope(data.payload);
    if (invalidScopes.length) errors.push(`Invalid scope for: ${invalidScopes.join(', ')}`)

    const invalidResourceScopes = await this.#validateResourceScope(data.payload);
    if (invalidResourceScopes.length) errors.push(`Invalid resource_scope for: ${invalidResourceScopes.join(', ')}`)

    if (errors.length) throw new Error(errors.join('; '))
  }

  async create(data, options, writer) {
    await this.validation(data)

    const { repo } = Mongo.getInstance()
    const result = await repo.client.create(data, options, writer);
    Client.logInfo({ data, options, writer }, result)

    // send to queue
    if (result.count) Client.publish({ msg: 'create' })

    return result;
  }

  async getBy(options) {
    const { repo } = Mongo.getInstance()
    const result = await repo.client.getBy(options);
    Client.logInfo({ options }, result)
    return result;
  }

  async getOneBy(options) {
    const { repo } = Mongo.getInstance()
    const result = await repo.client.getOneBy(options);
    Client.logInfo({ options }, result)
    return result;
  }

  async deleteBy(options, writer) {
    const { repo } = Mongo.getInstance()
    const result = await repo.client.deleteBy(options, writer);
    Client.logInfo({ options, writer }, result)

    // send to queue
    if (result.count) Client.publish({ msg: 'delete' })

    return result;
  }

  async updateBy(data, options, writer) {
    await this.validation(data)

    const { repo } = Mongo.getInstance()
    const result = await repo.client.updateBy(data, options, writer);
    Client.logInfo({ data, options, writer }, result)

    // send to queue
    if (result.count) Client.publish({ msg: 'update' })

    return result;
  }
}

module.exports = Client
