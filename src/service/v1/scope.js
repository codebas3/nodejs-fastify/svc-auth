/* eslint-disable class-methods-use-this */

const _ = require('lodash');
const { claims } = require('../../openid/option');
const { enumeration } = require('../../model/mongo/client');

const logger = require('../../logger/console')(__filename);
const { stringify, objError } = require('../../logger/lib/format');
const { constants } = require('../../config');
const Mongo = require('../../model/mongo');

const { TYPE_INFO, TYPE_ERROR } = constants

class Scope {
  static caller() {
    // * This is fake error for getting where the method call from;
    // * in case, we can't use arguments.caller in strict mode
    const e = new Error();
    return e.stack.split('\n')[3].trim().split(/\s/g)[1]
  }

  static logInfo(options, result) {
    logger.info(stringify(
      `${TYPE_INFO.SERVICE_V1}: ${Scope.caller()}`,
      ({
        returned: result.rows ? result.rows.length : result.length,
        options
      })
    ));
  }

  static logError(err) {
    logger.error(stringify(
      `${TYPE_ERROR.SERVICE_V1}: ${Scope.caller()}`,
      objError(err)
    ));
  }

  async getBy(options) {
    const { repo } = Mongo.getInstance()
    const result = await repo.resourceServer.getBy(options);
    Scope.logInfo({ options }, result)

    const mandatory = [...enumeration.scopes];

    let optional = _.concat(...Object.values(claims));
    mandatory.forEach(s => {
      optional = _.without(optional, s)
    })

    const resourceServer = []
    result.rows.forEach(row => {
      row.scopes.forEach(scope => {
        resourceServer.push(`${row.code}:${scope}`)
      })
    })

    return {
      count: 2,
      rows: [
        {
          field: 'scope',
          value: [
            { type: 'mandatory', data: mandatory },
            { type: 'optional', data: optional }
          ]
        },
        {
          key: 'resource_scope',
          value: [
            { type: 'resource_server', data: resourceServer }
          ]
        },
      ]
    }
  }
}

module.exports = Scope;
