module.exports = {
  HealthCheck: require('./health-check'),
  Client: require('./client'),
  ResourceServer: require('./resource-server'),
  Endpoint: require('./endpoint'),
  Role: require('./role'),
  Scope: require('./scope'),
};
