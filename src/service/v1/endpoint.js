/* eslint-disable class-methods-use-this */

const logger = require('../../logger/console')(__filename);
const { stringify, objError } = require('../../logger/lib/format');
const { rabbitmq, constants } = require('../../config');
const Mongo = require('../../model/mongo');
const { Publisher, Client: RMQ } = require('../../queue/rabbitmq');

const { TYPE_INFO, TYPE_ERROR } = constants

class Endpoint {
  static caller() {
    // * This is fake error for getting where the method call from;
    // * in case, we can't use arguments.caller in strict mode
    const e = new Error();
    return e.stack.split('\n')[3].trim().split(/\s/g)[1]
  }

  static logInfo(options, result) {
    logger.info(stringify(
      `${TYPE_INFO.SERVICE_V1}: ${Endpoint.caller()}`,
      ({
        returned: result.rows ? result.rows.length : result.length,
        options
      })
    ));
  }

  static logError(err) {
    logger.error(stringify(
      `${TYPE_ERROR.SERVICE_V1}: ${Endpoint.caller()}`,
      objError(err)
    ));
  }

  static async publish(message) {
    const rmq = RMQ.getInstance()
    const { Endpoint: q } = rabbitmq.queue;
    const publisher = new Publisher(rmq, q.exchange, rmq.channel)
    publisher.setOpts({ persistence: false, mandatory: true })
    publisher.publish(q.route, message, null, (err) => {
      if (err) this.logError(err)
    })
  }

  async create(data, options, writer) {
    const { repo } = Mongo.getInstance()
    const result = await repo.endpoint.create(data, options, writer);
    Endpoint.logInfo({ data, options, writer }, result)

    // send to queue
    if (result.count) Endpoint.publish({ msg: 'create' })

    return result;
  }

  async getBy(options) {
    const { repo } = Mongo.getInstance()
    const result = await repo.endpoint.getBy(options);
    Endpoint.logInfo({ options }, result)
    return result;
  }

  async getOneBy(options) {
    const { repo } = Mongo.getInstance()
    const result = await repo.endpoint.getOneBy(options);
    Endpoint.logInfo({ options }, result)
    return result;
  }

  async deleteBy(options, writer) {
    const { repo } = Mongo.getInstance()
    const result = await repo.endpoint.deleteBy(options, writer);
    Endpoint.logInfo({ options, writer }, result)

    // send to queue
    if (result.count) Endpoint.publish({ msg: 'delete' })

    return result;
  }

  async updateBy(data, options, writer) {
    const { repo } = Mongo.getInstance()
    const result = await repo.endpoint.updateBy(data, options, writer);
    Endpoint.logInfo({ data, options, writer }, result)

    // send to queue
    if (result.count) Endpoint.publish({ msg: 'update' })

    return result;
  }
}

module.exports = Endpoint
