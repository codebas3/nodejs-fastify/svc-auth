/* eslint-disable class-methods-use-this */

const logger = require('../../logger/console')(__filename);
const { stringify, objError } = require('../../logger/lib/format');
const { app, constants } = require('../../config');
const { random } = require('../../util/string');

const { TYPE_INFO, TYPE_ERROR } = constants;

class HealthCheck {
  isEmpty(obj) {
    return Object.keys(obj).length === 0;
  }

  caller() {
    // * This is fake error for getting where the method call from;
    // * in case, we can't use arguments.caller in strict mode
    const e = new Error();
    return e.stack.split('\n')[3].trim().split(/\s/g)[1]
  }

  logInfo(result) {
    logger.info(stringify(
      `${TYPE_INFO.SERVICE_V1}: ${this.caller()}`,
      ({
        returned: result
      })
    ));
  }

  logError(err) {
    logger.error(stringify(
      `${TYPE_ERROR.SERVICE_V1}: ${this.caller()}`,
      objError(err)
    ));
  }

  async checkRedis(manifest) {
    const { redis } = manifest;
    if (!redis) return null;

    try {
      const testKey = `${app.packageName}::HealthCheck`
      const testVal = random(10);

      await redis.client.set(testKey, testVal)
      await redis.client.get(testKey)

      return { name: 'redis', status: 'up', notes: `${testKey} => ${testVal}` }
    } catch (e) {
      this.logError(e)
      return { name: 'redis', status: 'down', notes: e.message }
    }
  }

  async checkSQL(manifest) {
    const { sql } = manifest;
    if (!sql) return null;

    try {
      const result = await sql.getDatetime()

      return { name: 'sql', status: 'up', notes: `getDatetime() => ${result}` }
    } catch (e) {
      this.logError(e)
      return { name: 'sql', status: 'down', notes: e.message }
    }
  }

  async checkMongoDB(manifest) {
    const { mongo } = manifest;
    if (!mongo) return null;
    const connections = {};

    try {
      Object.entries(mongo.mongoose.connections).forEach(([i, con]) => {
        connections[i] = {
          code: con._readyState,
          state: con.states[con._readyState]
        }
      })

      if (Object.values(connections).map(con => con.code).indexOf(1) === -1) {
        throw new Error('No one connected')
      }

      return { name: 'mongo', status: 'up', notes: connections }
    } catch (e) {
      this.logError(e)
      return { name: 'mongo', status: 'down', notes: connections }
    }
  }

  async checkGRPC(manifest) {
    const { grpc } = manifest;
    if (!grpc) return null;

    const { logs } = grpc;
    let status = grpc.app.servers.map(s => s.server.started)
    status = status[status.length - 1]

    return { name: 'grpc', status: status ? 'up' : 'down', notes: logs }
  }

  async checkAll() {
    const Manifest = require('../../manifest');
    const manifest = Manifest.getInstance();

    const result = [];
    const redis = await this.checkRedis(manifest);
    if (redis) result.push(redis)

    const sql = await this.checkSQL(manifest);
    if (sql) result.push(sql)

    const mongodb = await this.checkMongoDB(manifest);
    if (mongodb) result.push(mongodb);

    const grpc = await this.checkGRPC(manifest);
    if (grpc) result.push(grpc);

    this.logInfo(result)
    return result;
  }
}

module.exports = HealthCheck;
