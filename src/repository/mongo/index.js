module.exports = {
  client: require('./client'),
  resourceServer: require('./resource-server'),
  endpoint: require('./endpoint'),
  role: require('./role'),
}
