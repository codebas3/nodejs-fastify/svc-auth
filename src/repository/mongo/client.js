const { timezone, flattenObj } = require('../../util')
const Mongo = require('../../model/mongo')

const { mongoose } = Mongo.getInstance();
const existCondition = {
  $or: [
    { deletedAt: { $exists: false } },
    { deletedAt: { $not: { $gte: 0 } } },
  ]
}

async function getBy(options) {
  const model = mongoose.models.Client;
  const condition = {
    ...options.where,
    ...existCondition
  };
  const {
    count: _count, limit, skip, lean = true
  } = options;

  const count = _count ? await model.find(condition).countDocuments().exec() : undefined

  let query = model.find(condition);
  if (limit) query = query.limit(limit)
  if (skip) query = query.skip(skip)
  if (lean) query = query.lean(true)

  return {
    ...{ count },
    rows: await query.exec()
  };
}

async function getOneBy(options) {
  const Model = mongoose.models.Client;
  const condition = {
    ...options.where,
    ...existCondition
  };
  const { lean = true } = options;

  let query = Model.findOne(condition);
  if (lean) query = query.lean(true)

  const rows = await query.exec()
  return {
    count: rows ? 1 : 0,
    rows: [rows]
  };
}

async function create(data, options, writer) {
  const Model = mongoose.models.Client;
  const newData = new Model({
    ...data,
    createdBy: writer || 'SYSTEM'
  });

  await newData.save(options)

  return {
    count: 1,
    rows: [newData]
  }
}

async function updateBy(data, options, writer) {
  const Model = mongoose.models.Client;
  const condition = {
    ...options.where,
    ...existCondition
  };

  if (data._id) delete data._id
  if (data.payload?.client_id) delete data.payload.client_id

  const result = await Model.updateMany(condition, {
    ...flattenObj(data),
    updatedBy: writer || 'SYSTEM',
    updatedAt: timezone.unixNow()
  });

  return {
    count: result.modifiedCount || 0
  };
}

async function deleteBy(options, writer) {
  const Model = mongoose.models.Client;
  const condition = {
    ...options.where,
    ...existCondition
  };

  const result = await Model.updateMany(condition, {
    deletedBy: writer || 'SYSTEM',
    deletedAt: timezone.unixNow()
  });

  return {
    count: result.modifiedCount || 0
  };
}

module.exports = {
  getBy, getOneBy, create, updateBy, deleteBy
};
